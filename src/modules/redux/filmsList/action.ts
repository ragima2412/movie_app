import { IFilm } from '../filmDetail/types';
import { types } from './actionTypes';

export const setFilmsList = (payload: Array<IFilm>) => ({
    type: types.SET_FILMS_LIST,
    payload
});

export const setIsLoadingAC = (payload: boolean) => ({
    type: types.SET_IS_LOADNG,
    payload
});

export const setIsFiltered = (payload: boolean): { type: types.SET_IS_FILTERED, payload: boolean } => ({
    type: types.SET_IS_FILTERED,
    payload
});
export const setAllFilms = (payload: Array<IFilm>) => ({
    type: types.SET_ALL_FILMS,
    payload
});