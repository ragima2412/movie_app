import { types } from './actionTypes';
import { IFilmsReducer, IPayload } from './types';

const initialState = {
    filmsList: [],
    totalCount: 0,
    currentPage: 1,
    isLoading: false,
    isFiltered: false,
    filteredData: []
};

const filmsReducer = (state: IFilmsReducer = initialState, action: {type: string; payload: IPayload }) => {
    switch (action.type) {
        case types.SET_FILMS_LIST:
            return {
                ...state,
                filmsList: [...state.filmsList, ...action.payload.movies],
                totalCount: action.payload.totalCount,
                currentPage: state.currentPage + 1,
            };
        case types.SET_ALL_FILMS:
            return {
                ...state,
                filteredData: action.payload
            };
        case types.SET_IS_LOADNG:
            return {
                ...state,
                isLoading: action.payload
            };
        case types.SET_IS_FILTERED:
            return {
                ...state,
                isFiltered: action.payload
            };
        default: return state;
    }
};

export const selectFilms = (state: IFilmsReducer) => state.filmsList;
export const selectTotalCount = (state: IFilmsReducer) => state.totalCount;
export const selectCurrentPage = (state: IFilmsReducer) => state.currentPage;
export const selectIsLoadingFilms = (state: IFilmsReducer) => state.isLoading;
export const selectIsFiltered = (state: IFilmsReducer) => state.isFiltered;
export const selectFilteredData = (state: IFilmsReducer) => state.filteredData;

export default filmsReducer;