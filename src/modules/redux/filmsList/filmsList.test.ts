import filmsReducer from './reducer';
import { IFilmsReducer } from './types';
import { setAllFilms, setFilmsList, setIsFiltered, setIsLoadingAC } from './action';
import { selectCurrentPage, selectFilms, selectFilteredData, selectIsFiltered, selectIsLoading, selectTotalCount } from './selectors';

describe('filmsReducer', () => {
    const state: IFilmsReducer = {
        filmsList: [],
        totalCount: 0,
        currentPage: 1,
        isLoading: false,
        isFiltered: false,
        filteredData: []
    }

    const selectState = {
        filmsReducer: {
            filmsList: [],
            totalCount: 0,
            currentPage: 1,
            isLoading: false,
            isFiltered: false,
            filteredData: []
        }
    }

    it('set films list',() => {
        const films = filmsReducer(state, setFilmsList({
            currentPage: 1,
            movies: [{
                id: 1,
                poster_path: 'posterPath',
                genre_ids: [1,2],
                title: 'name',
            }],
            totalCount: 1,}))
        expect(films.filmsList).toEqual([{
            id: 1,
            poster_path: 'posterPath',
            genre_ids: [1,2],
            title: 'name',
        }])
        expect(films.currentPage).toEqual(2);
        expect(films.totalCount).toEqual(1);
    })

    it('set filtered films',() => {
        const films = filmsReducer(state, setAllFilms([{
            id: 1,
            poster_path: 'posterPath',
            genre_ids: [1,2],
            title: 'name'
        }]));
        expect(films.filteredData).toEqual([{
            id: 1,
            poster_path: 'posterPath',
            genre_ids: [1,2],
            title: 'name'
        }]);
    })

    it('loading bar',() => {
        const films = filmsReducer(state, setIsLoadingAC(true));
        expect(films.isLoading).toBeTruthy;
    })

    it('data is filtered',() => {
        const films = filmsReducer(state, setIsFiltered(true));
        expect(films.isFiltered).toBeTruthy;
    })

    it('selectFilms test', () => {
        const select = selectFilms(selectState);
        expect(select).toEqual(selectState.filmsReducer.filmsList);
    })

    it('selectTotalCount test', () => {
        const select = selectTotalCount(selectState);
        expect(select).toEqual(selectState.filmsReducer.totalCount);
    })

    it('selectCurrentPage test', () => {
        const select = selectCurrentPage(selectState);
        expect(select).toEqual(selectState.filmsReducer.currentPage);
    })

    it('selectIsLoading test', () => {
        const select = selectIsLoading(selectState);
        expect(select).toEqual(selectState.filmsReducer.isLoading);
    })

    it('selectIsFiltered test', () => {
        const select = selectIsFiltered(selectState);
        expect(select).toEqual(selectState.filmsReducer.isFiltered);
    })

    it('selectFilteredData test', () => {
        const select = selectFilteredData(selectState);
        expect(select).toEqual(selectState.filmsReducer.filteredData);
    })

    it('setFilmsList test', () => {
        const newReleasesList = setFilmsList([{
            id: 1,
            poster_path: '',
            genre_ids: [1,2],
            title: ''
        }]);
        expect(newReleasesList).toEqual({type:'SET_FILMS_LIST', payload: [{
            id: 1,
            poster_path: '',
            genre_ids: [1,2],
            title: ''
        }]})
    });

    it('setAllFilms test', () => {
        const newReleasesList = setAllFilms([{
            id: 1,
            poster_path: '',
            genre_ids: [1,2],
            title: ''
        }]);
        expect(newReleasesList).toEqual({type:'SET_ALL_FILMS', payload: [{
            id: 1,
            poster_path: '',
            genre_ids: [1,2],
            title: ''
        }]})
    });

    it('setIsLoadingAC test', () => {
        const issLoading = setIsLoadingAC(true);
        expect(issLoading).toEqual({type:'SET_IS_LOADING', payload: true});
    });

    it('setIsFiltered test', () => {
        const issLoading = setIsFiltered(true);
        expect(issLoading).toEqual({type:'SET_IS_FILTERED', payload: true});
    });

})