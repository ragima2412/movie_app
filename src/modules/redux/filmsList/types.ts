import { IFilm } from "../filmDetail/types";

export interface IFilmsReducer {
    filmsList: Array<IFilm>,
    totalCount: number,
    currentPage: number,
    isLoading: boolean,
    isFiltered: boolean,
    filteredData: Array<IFilm>
}
export interface IState {
    filmsReducer: {
        filmsList: Array<IFilm>,
        totalCount: number,
        currentPage: number,
        isLoading: boolean,
        isFiltered: boolean,
        filteredData: Array<IFilm>
    },
}
export interface IPayload {
    currentPage: number,
    movies: Array<IFilm>,
    totalCount: number,
}