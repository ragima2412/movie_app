import { actionTypesSignUp } from './actionTypes';

export type setMessageType = {
    type: typeof actionTypesSignUp.SET_MESSAGE,
    payload: { login: string, password: string, message: string }
}

export const setMessageAC = (payload: { login: string, password: string, message: string }): setMessageType => ({
    type: actionTypesSignUp.SET_MESSAGE,
    payload,
});

export const setModalSignUp = (payload: boolean) : {type: actionTypesSignUp.SET_MODAL_SIGN_UP , payload : boolean} => ({
    type: actionTypesSignUp.SET_MODAL_SIGN_UP,
    payload
});

export const setIsLoadingMessageAC = (payload: boolean) : {type: actionTypesSignUp.SET_IS_LOADING_MESSAGE, payload: boolean} => ({
    type: actionTypesSignUp.SET_IS_LOADING_MESSAGE,
    payload
});

export const setLoginSignUp = (payload: string) : { type: actionTypesSignUp.SET_LOGIN_SIGN_UP, payload: string } => ({
    type: actionTypesSignUp.SET_LOGIN_SIGN_UP,
    payload
});

export const setFirstNameSignUp = (payload: string) : { type: actionTypesSignUp.SET_FIRST_NAME_SIGN_UP, payload: string } => ({
    type: actionTypesSignUp.SET_FIRST_NAME_SIGN_UP,
    payload
});

export const setLastNameSignUp = (payload: string) : { type: actionTypesSignUp.SET_LAST_NAME_SIGN_UP, payload: string } => ({
    type: actionTypesSignUp.SET_LAST_NAME_SIGN_UP,
    payload
});

export const setPasswordSignUp = (payload: string) : {type: actionTypesSignUp.SET_PASSWORD_SIGN_UP, payload: string} => ({
    type: actionTypesSignUp.SET_PASSWORD_SIGN_UP,
    payload
});

export const setEmptyMessageSignUp = (payload: object):{  type: actionTypesSignUp.SET_EMPTY_MESSAGE_SIGN_UP, payload: object} => ({
    type: actionTypesSignUp.SET_EMPTY_MESSAGE_SIGN_UP,
    payload
});
