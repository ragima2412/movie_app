import { IState } from '../signUp/types';
import { setMessageAC } from '../signUp/action';
import signUpReducer from './reducer';
import { ISignUpReducer } from './types';
import { messageSelector } from './selectors';

const state: ISignUpReducer = {
    userInfo: {
        first_name: '',
        last_name: '',
        login: '',
        password: ''
    },
    message: {
        first_name: '',
        last_name: '',
        login: '',
        password: '',
        message: '123',
    },
    modalWindow: false,
    isLoadingMessage: false
};

const state1: IState = {
    signUpReducer: {
        userInfo: {
            first_name: '',
            last_name: '',
            login: '',
            password: ''
        },
        message: {
            first_name: '',
            last_name: '',
            login: '',
            password: '',
            message: '',
        },
        modalWindow: false,
        isLoadingMessage: false
    }
};

describe('SignUpReducer', () => {
    test('Set message', () => {
        const testMessage = signUpReducer(state, setMessageAC({
            login: '',
            password: '',
            message: ''
        }));
        expect(testMessage.message).toEqual({
            login: '',
            password: '',
            message: ''
        });
    });
    test('Set default state', () => {
        const testDefaultState = signUpReducer(state, { type: '' });
        expect(testDefaultState).toEqual(state);
    });
    test('message selector', () => {
        const message = messageSelector(state1);
        expect(message).toEqual(state1.signUpReducer.message);
    });
    test('setMessageAC test', () => {
        const message = setMessageAC({ login: '', password: '', message: '' });
        expect(message).toEqual({type: 'SET_MESSAGE', payload: {login: '', password: '', message: ''}});
    });
});