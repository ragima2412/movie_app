export interface IState {
    signUpReducer: {
        message: IPayload,
        modalWindow: boolean,
        isLoadingMessage: boolean,
        userInfo: IPayloadUserInfo
    }
}

export interface ISignUpReducer {
    message: IPayload
    modalWindow: boolean
    isLoadingMessage: boolean
    userInfo: IPayloadUserInfo
}

export interface IPayload {
    first_name: string,
    last_name: string,
    login: string,
    password: string,
    message: string,
}

export interface IPayloadUserInfo {
    first_name: string,
    last_name: string,
    login: string,
    password: string,
}