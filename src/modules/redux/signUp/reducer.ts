import { actionTypesSignUp } from './actionTypes';
import { IPayload, IPayloadUserInfo, ISignUpReducer } from './types';

const initialState = {
    userInfo: {
        first_name: '',
        last_name: '',
        login: '',
        password: ''
    },
    message: {
        first_name: '',
        last_name: '',
        login: '',
        password: '',
        message: '',
    },
    modalWindow: false,
    isLoadingMessage: false
};

const signUpReducer = (state: ISignUpReducer = initialState, action: { type: string; payload?: IPayload | IPayloadUserInfo | { login: string, password: string, message: string }}) => {
    switch (action.type) {
        case actionTypesSignUp.SET_MESSAGE:
            return {
                ...state,
                message: action.payload,
            };
        case actionTypesSignUp.SET_EMPTY_MESSAGE:
            return {
                ...state,
                message: action.payload
            };
        case actionTypesSignUp.SET_MODAL_SIGN_UP:
            return {
                ...state,
                modalWindow: action.payload
            };
        case actionTypesSignUp.SET_IS_LOADING_MESSAGE:
            return {
                ...state,
                isLoadingMessage: action.payload
            };
        case actionTypesSignUp.SET_LOGIN_SIGN_UP: 
            return {
                ...state,
                userInfo: {...state.userInfo, login: action.payload}
            };
        case actionTypesSignUp.SET_FIRST_NAME_SIGN_UP:
            return {
                ...state,
                userInfo: {...state.userInfo, first_name: action.payload}
            };
        case actionTypesSignUp.SET_LAST_NAME_SIGN_UP:
            return {
                ...state,
                userInfo: {...state.userInfo, last_name: action.payload}
            };
        case actionTypesSignUp.SET_PASSWORD_SIGN_UP:
            return {
                ...state,
                userInfo: {...state.userInfo, password: action.payload}
            };
        case actionTypesSignUp.SET_EMPTY_MESSAGE_SIGN_UP: {
            return {
                ...state,
                userInfo: {...state.userInfo, first_name: '', last_name: '', login: '', password: ''}
            };
        }
        default: return state;
    }
};

export const messageSelector = (state: ISignUpReducer) => state.message;
export const modalSelector = (state: ISignUpReducer) => state.modalWindow;
export const isLoadingMessage = (state: ISignUpReducer) => state.isLoadingMessage;
export const isUserInfo = (state: ISignUpReducer) => state.userInfo;

export default signUpReducer;