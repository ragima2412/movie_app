import { filterTypesRedux } from "./actionTypes";
import { Action } from 'redux';

export const setLanguageAC = (payload: string) => ({
    type: filterTypesRedux.SET_LANGUAGES,
    payload
});
export const setBudgetAC = (payload: Array<string>) => ({
    type: filterTypesRedux.SET_BUDGET,
    payload
});
export const setPopularAC = (payload: Array<string>) => ({
    type: filterTypesRedux.SET_POPULAR_FILTERS,
    payload
});
export const setRevenueAC = (payload: Array<string>) => ({
    type: filterTypesRedux.SET_REVENUE,
    payload
});
export const setClear = (): Action<filterTypesRedux.SET_CLEAR> => ({
    type: filterTypesRedux.SET_CLEAR
});
export const setSearchAC =(payload: string) => ({
    type: filterTypesRedux.SET_SEARCH,
    payload
});
export const setYearAC =(payload: Array<string>) => ({
    type:filterTypesRedux.SET_YEAR,
    payload
});

export const setClearSearch = () : Action<filterTypesRedux.SET_CLEAR_SEARCH> => ({
    type: filterTypesRedux.SET_CLEAR_SEARCH
});