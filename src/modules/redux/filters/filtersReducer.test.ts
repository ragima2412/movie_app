import { setBudgetAC, setClear, setLanguageAC, setPopularAC, setRevenueAC, setSearchAC, setYearAC } from './action';
import filtersReducer from './reducer';
import { selectBudget, selectLanguage, selectPopularFilter, selectRevenue, selectSearch, selectYear } from '../rootReducer';
import { IFiltersReducer, IStateFilters } from './types';

const state: IFiltersReducer = {
    value: ['Languages', ''],
    budget: ['Budget', ''],
    popular: ['Popular', ''],
    revenue: ['Revenue', ''],
    search: '',
    years: ['', '']
}

const state1: IStateFilters = {
    filtersReducer: {
        value: ['Languages', ''],
        budget: ['Budget', ''],
        popular: ['Popular', ''],
        revenue: ['Revenue', ''],
        search: '',
        years: ['', '']
    }
}

describe("FiltersReducer Testing", () => {
    test('selectLanguage test', () => {
        const lang = selectLanguage(state1)
        expect(lang).toEqual(state1.filtersReducer.value)
    })
    test('selectBudget test', () => {
        const budget = selectBudget(state1)
        expect(budget).toEqual(state1.filtersReducer.budget)
    })
    test('selectPopularity test', () => {
        const popular = selectPopularFilter(state1)
        expect(popular).toEqual(state1.filtersReducer.popular)
    })
    test('selectRevenue test', () => {
        const revenue = selectRevenue(state1)
        expect(revenue).toEqual(state1.filtersReducer.revenue)
    })
    test('selectSearch test', () => {
        const search = selectSearch(state1)
        expect(search).toEqual(state1.filtersReducer.search)
    })
    test('selectYear test', () => {
        const year = selectYear(state1)
        expect(year).toEqual(state1.filtersReducer.years)
    })
    test("Budget test", () => {
        const newBudget = filtersReducer(state, setBudgetAC(['100', '200']))
        expect(newBudget.budget).toEqual(['100', '200']);
    });

    test("Language Test", () => {
        const newLanguage = filtersReducer(state, setLanguageAC('en'));
        expect(newLanguage.value[0]).toEqual('');
        expect(newLanguage.value[1]).toEqual('en');
    });

    test("Year Test", () => {
        const newYear = filtersReducer(state, setYearAC(['01-01-2021', '01-01-2021']));
        expect(newYear.years[0]).toEqual('01-01-2021');
        expect(newYear.years[1]).toEqual('01-01-2021');
    });

    test("Popular Test", () => {
        const newPopular = filtersReducer(state, setPopularAC(['100', '200']));
        expect(newPopular.popular[0]).toEqual('100');
        expect(newPopular.popular[1]).toEqual('200');
    });

    test("Revenue Test", () => {
        const newRevenue = filtersReducer(state, setRevenueAC(['1000', '2000']));
        expect(newRevenue.revenue[0]).toEqual("1000");
        expect(newRevenue.revenue[1]).toEqual('2000');
    });

    test("Search Test", () => {
        const newSearch = filtersReducer(state, setSearchAC('search'));
        expect(newSearch.search).toEqual('search');
    });

    test("Initial Test", () => {
        const newInitial = filtersReducer(state, setClear());
        expect(newInitial).toEqual(state);
    });

    test("Default Test", () => {
        const newDefault = filtersReducer(state, { type: 'testDefaultCase' });
        expect(newDefault).toEqual(state);
    });

    test('setLanguageAC test', () => {
        const lang = setLanguageAC('')
        expect(lang).toEqual({ type: 'SET_LANGUAGES', payload: '' })
    });

    test('setBudgetAC test', () => {
        const budget = setBudgetAC(['', ''])
        expect(budget).toEqual({ type: 'SET_BUDGET', payload: ['', ''] })
    });

    test('setPopularAC test', () => {
        const popular = setPopularAC([''])
        expect(popular).toEqual({ type: 'SET_POPULAR_FILTERS', payload: [''] })
    });

    test('setRevenueAC test', () => {
        const revenue = setRevenueAC([''])
        expect(revenue).toEqual({ type: 'SET_REVENUE', payload: [''] })
    })

    test('setClear', () => {
        const clear = setClear()
        expect(clear).toEqual({ type: 'SET_CLEAR' })
    })

    test('setSearchAC test', () => {
        const search = setSearchAC('')
        expect(search).toEqual({type: 'SET_SEARCH', payload: ''})
    })

    test('setYearAC test', () => {
        const year = setYearAC([''])
        expect(year).toEqual({ type: 'SET_YEAR', payload: [''] })
    })
});