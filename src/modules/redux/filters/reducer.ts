import { filterTypesRedux } from "./actionTypes";
import { IFiltersReducer } from "./types";

const initialState = {
    value: ['Languages',''],
    budget: ['Budget',''],
    popular: ['Popular',''],
    revenue: ['Revenue',''],
    search: '',
    years: ['','']
};

const filtersReducer = (state: IFiltersReducer = initialState, action: { type: string, payload?: any }) => {
    switch (action.type) {
        case filterTypesRedux.SET_BUDGET:
            return {
                ...state,
                budget: action.payload
            };
        case filterTypesRedux.SET_LANGUAGES:
            return {
                ...state,
                value: ['', action.payload]
            };
        case filterTypesRedux.SET_YEAR:
            return {
                ...state,
                years: [action.payload[0], action.payload[1]]
            };
        case filterTypesRedux.SET_POPULAR_FILTERS:
            return {
                ...state,
                popular: action.payload
            };
        case filterTypesRedux.SET_REVENUE:
            return {
                ...state,
                revenue: action.payload
            };
        case filterTypesRedux.SET_SEARCH:
            return {
                ...state,
                search: action.payload
            };
        case filterTypesRedux.SET_CLEAR_SEARCH:
            return {
                ...state,
                search: ''
            };
        case filterTypesRedux.SET_CLEAR:
            return initialState;
        default: return state;
    }
};

export const selectLanguage = (state: IFiltersReducer) => state.value;
export const selectBudget = (state: IFiltersReducer) => state.budget;
export const selectPopularFilter = (state: IFiltersReducer) => state.popular;
export const selectRevenue = (state: IFiltersReducer) => state.revenue;
export const selectSearch = (state: IFiltersReducer) => state.search;
export const selectYear = (state: IFiltersReducer) => state.years;

export default filtersReducer;