export interface IFiltersReducer {
    value: Array<string>,
    budget: Array<string>,
    popular: Array<string>,
    revenue: Array<string>,
    search: string,
    years: Array<string>
}

export interface IStateFilters {
    filtersReducer: {
        value: Array<string>,
        budget: Array<string>,
        popular: Array<string>,
        revenue: Array<string>,
        search: string,
        years: Array<string>
    }
}
