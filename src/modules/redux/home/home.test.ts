import { setIsLoadingAC, setNewReleasesList, setPopularList, setRandomList } from './action';
import sliderReducer from './reducer';
import { selectIsLoading, selectNewReleases, selectPopular, selectRandom } from './selectors';
import { ISliderReducer } from './types';

describe('sliderReducer', () => {

    const state : ISliderReducer = {
        popularList: [],
        newReleasesList: [],
        randomList: [],
        isLoading: false
    }

    const selectState = {
        sliderReducer: {
            popularList: [],
            newReleasesList: [],
            randomList: [],
            isLoading: false
        }
    }

    it('set popular films',() => {
        const slider = sliderReducer(state, setPopularList([{
            poster_path: 'posterPath',
            genre_ids: [1,2],
            title: 'name',
        }]))
        expect(slider.popularList).toEqual([{
            poster_path: 'posterPath',
            genre_ids: [1,2],
            title: 'name',
        }])
    })

    it('set new films',() => {
        const slider = sliderReducer(state, setNewReleasesList([{
            poster_path: 'posterPath',
            genre_ids: [1,2],
            title: 'name',
        }]))
        expect(slider.newReleasesList).toEqual([{
            poster_path: 'posterPath',
            genre_ids: [1,2],
            title: 'name',
        }])
    })

    it('set random films',() => {
        const slider = sliderReducer(state, setRandomList([{
            poster_path: 'posterPath',
            genre_ids: [1,2],
            title: 'name',
        }]))
        expect(slider.randomList).toEqual([{
            poster_path: 'posterPath',
            genre_ids: [1,2],
            title: 'name',
        }])
    })

    it('loading bar',() => {
        const slider = sliderReducer(state, setIsLoadingAC(true));
        expect(slider.isLoading).toEqual(true);
    })

    it('default',() => {
        const defaultCase = sliderReducer(state, {type: 'a'});
        expect(defaultCase).toEqual(state);
    })

    it('selectRandom test', () => {
       const select = selectRandom(selectState);
       expect(select).toEqual(selectState.sliderReducer.randomList);
    })

    it('selectPopular test', () => {
        const select = selectPopular(selectState);
        expect(select).toEqual(selectState.sliderReducer.popularList);
    })

    it('selectNewReleases test', () => {
        const select = selectNewReleases(selectState);
        expect(select).toEqual(selectState.sliderReducer.newReleasesList);
    })

    it('selectIsLoading test', () => {
        const select = selectIsLoading(selectState);
        expect(select).toEqual(selectState.sliderReducer.isLoading);
    })

    it('setRandomList test', () => {
        const randomList = setRandomList([{
            poster_path: '',
            genre_ids: [1,2],
            title: ''
        }]);
        expect(randomList).toEqual({type:'SET_RANDOM_LIST', payload: [{
            poster_path: '',
            genre_ids: [1,2],
            title: ''
        }]})
    });

    it('setPopularList test', () => {
        const popularList = setPopularList([{
            poster_path: '',
            genre_ids: [1,2],
            title: ''
        }]);
        expect(popularList).toEqual({type:'SET_POPULAR_LIST', payload: [{
            poster_path: '',
            genre_ids: [1,2],
            title: ''
        }]})
    });

    it('setNewReleasesList test', () => {
        const newReleasesList = setNewReleasesList([{
            poster_path: '',
            genre_ids: [1,2],
            title: ''
        }]);
        expect(newReleasesList).toEqual({type:'SET_NEW_RELEASES_LIST', payload: [{
            poster_path: '',
            genre_ids: [1,2],
            title: ''
        }]})
    });

    it('setIsLoadingAC test', () => {
        const issLoading = setIsLoadingAC(true);
        expect(issLoading).toEqual({type:'SET_IS_LOADING', payload: true});
    });

})