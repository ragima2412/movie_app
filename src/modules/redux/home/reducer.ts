import { actionTypesHome } from './actionTypes';
import { IFilm } from '../filmDetail/types';
import { ISliderReducer } from './types';

const initialState = {
    popularList: [],
    newReleasesList: [],
    randomList: [],
    isLoading: false,
    homeLink: false,
    moviesLink: false
};

const sliderReducer = (state: ISliderReducer = initialState, action: {type: string; payload?: Array<IFilm> | boolean} ) => {
    switch (action.type) {
        case actionTypesHome.SET_POPULAR_LIST:
            return {
                ...state,
                popularList: action.payload
            };
        case actionTypesHome.SET_NEW_RELEASES_LIST:
            return {
                ...state,
                newReleasesList: action.payload
            };
        case actionTypesHome.SET_RANDOM_LIST:
                return {
                    ...state,
                    randomList: action.payload
            };
        case actionTypesHome.SET_IS_LOADING: 
            return {
                ...state,
                isLoading: action.payload
            };
        case actionTypesHome.SET_ACTIVE_HOME_LINK: 
            return {
                ...state,
                homeLink: action.payload
            };
        case actionTypesHome.SET_ACTIVE_MOVIES_LINK: 
            return {
                ...state,
                moviesLink: action.payload
            };
        default: return state;
    }
};

export const selectPopular = (state: ISliderReducer) => state.popularList;
export const selectNewReleases = (state: ISliderReducer) => state.newReleasesList;
export const selectRandom = (state: ISliderReducer) => state.randomList;
export const selectIsLoading = (state: ISliderReducer) => state.isLoading;
export const selectHomeLink = (state: ISliderReducer) => state.homeLink;
export const selectMoviesLink = (state: ISliderReducer) => state.moviesLink;

export default sliderReducer;