export enum actionTypesHome {
    SET_POPULAR_LIST = 'SET_POPULAR_LIST',
    SET_NEW_RELEASES_LIST = 'SET_NEW_RELEASES_LIST',
    SET_RANDOM_LIST = 'SET_RANDOM_LIST',
    SET_IS_LOADING = 'SET_IS_LOADING',
    SET_ACTIVE_HOME_LINK = 'SET_ACTIVE_HOME_LINK',
    SET_ACTIVE_MOVIES_LINK = 'SET_ACTIVE_MOVIES_LINK'
}
