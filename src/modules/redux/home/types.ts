import { IFilm } from "../filmDetail/types";
export interface ISliderReducer {
    popularList: Array<IFilm>,
    newReleasesList: Array<IFilm>,
    randomList: Array<IFilm>,
    isLoading: boolean,
    homeLink: boolean,
    moviesLink: boolean
}
export interface IState {
    sliderReducer: {
        popularList: Array<IFilm>,
        newReleasesList: Array<IFilm>,
        randomList: Array<IFilm>,
        isLoading: boolean,
        homeLink: boolean,
        moviesLink: boolean
    },
}
export interface IPayload {
    currentPage: number;
    movies: Array<IFilm>
    totalCount: number;
}