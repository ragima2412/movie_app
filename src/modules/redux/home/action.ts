import { IFilm } from '../filmDetail/types';
import { actionTypesHome } from './actionTypes';

export const setPopularList = (payload: Array<IFilm>) => ({
    type: actionTypesHome.SET_POPULAR_LIST,
    payload,
});

export const setNewReleasesList = (payload: Array<IFilm>) => ({
    type: actionTypesHome.SET_NEW_RELEASES_LIST,
    payload,
});

export const setRandomList = (payload: Array<IFilm>) => ({
    type: actionTypesHome.SET_RANDOM_LIST,
    payload,
});

export const setIsLoadingAC = (payload: boolean) => ({
    type: actionTypesHome.SET_IS_LOADING,
    payload
});

export const setActiveHomeLink = (payload: boolean) : { type: actionTypesHome.SET_ACTIVE_HOME_LINK, payload: boolean } => ({
    type: actionTypesHome.SET_ACTIVE_HOME_LINK,
    payload
});

export const setActiveMoviesLink = (payload: boolean) : { type: actionTypesHome.SET_ACTIVE_MOVIES_LINK, payload: boolean } => ({
    type: actionTypesHome.SET_ACTIVE_MOVIES_LINK,
    payload
});