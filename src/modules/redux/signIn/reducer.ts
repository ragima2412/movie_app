import { actionTypes } from './actionTypes';
import { IsignInReducer, IStateSignIn } from './types';

const initialState = {
    login: '',
    password: '',
    token: '',
    message: {
        login:'',
        password:'',
        message:'',
    },
    userId: '',
    modalWindow: false,
    logOut: false,
    activeButton: false,
    isLoadingMessage: false
};

const signInReducer = (state: IsignInReducer = initialState, action: {type:string; payload?: string | {login: string, password: string, message: string, first_name?: string, last_name?: string} | number  }) => {
    switch (action.type) {
        case actionTypes.SET_TOKEN:
            return {
                ...state,
                token: action.payload
            };
        case actionTypes.SET_MESSAGE_SIGN_IN: 
            return {
                ...state,
                message: action.payload
            };
        case actionTypes.SET_USER_ID: 
            return {
                ...state,
                userId: action.payload
            };
        case actionTypes.SET_LOGIN: 
            return {
                ...state,
                login: action.payload
            };
        case actionTypes.SET_INITIAL_DATA:
            return initialState;
        case actionTypes.SET_PASSWORD:
            return {
                ...state,
                password: action.payload
            };
        case actionTypes.SET_LOG_OUT:
            return {
                ...state,
                logOut: action.payload
            };
        case actionTypes.SET_EMPTY_MESSAGE_SIGN_IN: 
            return {
                ...state,
                message: {
                    login: action.payload,
                    password: action.payload,
                }
            };
        case actionTypes.SET_MODAL_SIGN_IN: 
            return {
                ...state,
                modalWindow: action.payload
            };
        case actionTypes.SET_ACTIVE_BUTTON: 
            return {
                ...state,
                activeButton: action.payload
            };
        case actionTypes.SET_IS_LOADING_MESSAGE_SIGN_IN:
            return {
                ...state,
                isLoadingMessage: action.payload
            };
        case actionTypes.SET_EMPTY_MESSAGE_SIGN_IN_INPUTS: 
            return {
                ...state,
                login: '',
                password: '',
            };
        default: return state;
    }
};

export const getUserProfile = (state: IStateSignIn) => state;
export const getUserData = (state: IStateSignIn) => state.message;
export const getModalWindow = (state: IStateSignIn) => state.modalWindow;
export const selectLogOut = (state: IStateSignIn): boolean => state.logOut;
export const setActiveButton = (state: IStateSignIn) => state.activeButton;
export const isLoadingMessageSignIn = (state: IStateSignIn) => state.isLoadingMessage;

export default signInReducer;