import { actionTypes } from './actionTypes';

export const setTokenAC = (payload: string) => ({
    type: actionTypes.SET_TOKEN,
    payload
});

export const setMessageSignIn = (payload: { login: string, password: string, message: string }) : { type: actionTypes.SET_MESSAGE_SIGN_IN, payload: {login: string, password: string, message: string, first_name?: string, last_name?: string} }=> ({
    type: actionTypes.SET_MESSAGE_SIGN_IN,
    payload
});

export const setUserIdAC = (payload: number) : {type: actionTypes.SET_USER_ID, payload: number} => ({
    type: actionTypes.SET_USER_ID,
    payload
});

export const setLoginAC = (payload: string) : { type: actionTypes.SET_LOGIN, payload: string }=> ({
    type: actionTypes.SET_LOGIN,
    payload
});

export const setInitialDataAC = () : { type: actionTypes.SET_INITIAL_DATA }=> ({
    type: actionTypes.SET_INITIAL_DATA
});

export const setPasswordAC = (payload: string) : {type: actionTypes.SET_PASSWORD, payload: string} => ({
    type: actionTypes.SET_PASSWORD,
    payload
});

export const setEmptyMessageSignInAC = (payload: string) : {type: actionTypes.SET_EMPTY_MESSAGE_SIGN_IN, payload: string} => ({
    type: actionTypes.SET_EMPTY_MESSAGE_SIGN_IN,
    payload
});

export const setModalSignIn = (payload: boolean) : {type: actionTypes.SET_MODAL_SIGN_IN, payload: boolean}=> ({
    type: actionTypes.SET_MODAL_SIGN_IN,
    payload
});

export const setLogOut = (payload: boolean) : {type: actionTypes.SET_LOG_OUT, payload: boolean} => ({
    type: actionTypes.SET_LOG_OUT,
    payload
});

export const setActiveButtonAC = (payload: boolean) : { type: actionTypes.SET_ACTIVE_BUTTON, payload: boolean} => ({
    type: actionTypes.SET_ACTIVE_BUTTON,
    payload
});

export const setIsLoadingMessageSignIn = (payload: boolean) : { type: actionTypes.SET_IS_LOADING_MESSAGE_SIGN_IN, payload: boolean}=> ({
    type: actionTypes.SET_IS_LOADING_MESSAGE_SIGN_IN,
    payload
});

export const setEmptyMessageSignInInputs = (payload: object) : { type: actionTypes.SET_EMPTY_MESSAGE_SIGN_IN_INPUTS, payload: object} => ({
    type: actionTypes.SET_EMPTY_MESSAGE_SIGN_IN_INPUTS,
    payload
});
