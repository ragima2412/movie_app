import { setEmptyMessageSignInAC, setInitialDataAC, setLoginAC, setMessageSignIn, setPasswordAC, setTokenAC, setUserIdAC } from "./action";
import signInReducer from "./reducer";
import { getUserData, getUserProfile } from "./selectors";
import { IsignInReducer, IStateSignIn } from "./types";

const state: IsignInReducer = {
    login: '',
    token: '',
    message: {
        login: '',
        password: '',
        message: '',
    },
    userId: '',
    password: '',
    modalWindow: false,
    activeButton: false,
    isLoadingMessage: false,
    logOut: false,
};

const state1: IStateSignIn = {
    signInReducer: {
        login: 'login',
        token: 'token',
        message: {
            login: 'login',
            password: 'password',
            message: 'message',
            modalWindow: false
        },
        userId: 'userId',
        password: 'password',
        modalWindow: false,
        logOut: false,
        activeButton: false,
        isLoadingMessage: false
    }
};

describe('SignInReducer test', () => {
    test('Token Test', () => {
        const newToken = signInReducer(state, setTokenAC('token'));
        expect(newToken.token).toEqual('token');
    });
    test('Message Test', () => {
        const newMessage = signInReducer(state, setMessageSignIn({
            login: 'test',
            password: 'test',
            message: 'test',
            // first_name: 'test',
            // last_name: 'test'
        }));
        expect(newMessage.message).toEqual({
            login: 'test',
            password: 'test',
            message: 'test',
            first_name: 'test',
            last_name: 'test'
        });
    });
    test('Set userId Test', () => {
        const testUserId = signInReducer(state, setUserIdAC(1));
        expect(testUserId.userId).toEqual(1);
    });
    test('Set login', () => {
        const testLogin = signInReducer(state, setLoginAC('login'));
        expect(testLogin.login).toEqual('login');
    });
    test('Set password', () => {
        const testPassword = signInReducer(state, setPasswordAC('password'));
        expect(testPassword.password).toEqual('password');
    });
    test('Set initial data', () => {
        const testInitialData = signInReducer(state, setInitialDataAC());
        expect(testInitialData).toEqual(state);
    });
    test('Default state', () => {
        const testDefaultState = signInReducer(state, {type: ''});
        expect(testDefaultState).toEqual(state);
    });
    test('getUserData selector', () => {
        const userData = getUserData(state1);
        expect(userData).toEqual(state1.signInReducer.message);
    });
    test('getSignInReducer', () => {
        const userProfile = getUserProfile(state1);
        expect(userProfile).toEqual(state1.signInReducer);
    });
    test('setTokenAC test', () => {
        const token = setTokenAC('');
        expect(token).toEqual({type: 'SET_TOKEN', payload: ''});
    });
    test('setMessageAC test', () => {
        const message = setMessageSignIn({ login: '', password: '', message: '' });
        expect(message).toEqual({type: 'SET_MESSAGE_SIGN_IN', payload: { login: '', password: '', message: '', first_name: '', last_name: ''}});
    });
    test('setUserIdAC', () => {
        const userId = setUserIdAC(1);
        expect(userId).toEqual({type: 'SET_USER_ID', payload: 1});
    });
    test('setLoginAC test', () => {
        const login = setLoginAC('');
        expect(login).toEqual({type: 'SET_LOGIN', payload: ''});
    });
    test('setInitialData', () => {
        const initialData = setInitialDataAC();
        expect(initialData).toEqual({type: 'SET_INITIAL_DATA'});
    });
    test('setPasswordAC test', () => {
        const password = setPasswordAC('');
        expect(password).toEqual({type: 'SET_PASSWORD', payload: ''});
    });
    test('setEmptyMessageAC test', () => {
        const emptyMessage = setEmptyMessageSignInAC('');
        expect(emptyMessage).toEqual({type: 'SET_EMPTY_MESSAGE_SIGN_IN', payload: ''});
    });
});