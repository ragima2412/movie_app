export interface IsignInReducer {
    login: string,
    token: string,
    message: {
        login: string,
        password: string,
        message: string
    },
    userId: string,
    password: string,
    modalWindow: boolean,
    activeButton: boolean,
    isLoadingMessage: boolean
    logOut: boolean,
}

//CHANGED TYPES
export interface IStateSignIn {
    login: string,
    token: string,
    message: {
        login: string,
        password: string,
        message: string,
        modalWindow: boolean
    },
    userId: string,
    password: string,
    modalWindow: boolean,
    logOut: boolean,
    activeButton: boolean,
    isLoadingMessage: boolean
}