import { setGenresList, setLanguagesList } from "./action";
import commonReducer from "./reducer";
import { ICommonReducerSelector, selectGenres } from "./selectors";
import { ICommonReducer } from "./types";

const state: ICommonReducer = {
    genres: [],
    languages: [],
};

const state1 : ICommonReducerSelector = {
    commonReducer : {
        genres: [],
        languages: [],
    }
};

describe('CommonReducer Test', () => {
    test('selectGenres test',() => {
        const genres = selectGenres(state1);
        expect(genres).toEqual(state1.commonReducer.genres);
    });

    test('selectLanguages test',() => {
        const lang = selectGenres(state1);
        expect(lang).toEqual(state1.commonReducer.languages);
    });

    test ('Genres Test', () => {
        const newGenres = commonReducer(state, setGenresList({}));
        expect(newGenres.genres).toEqual({});
    });;

    test ('Languages Test', () => {
        const newlanguages = commonReducer(state, setLanguagesList([{
            name:'English',
            value: 'en'
        }]));
        expect(newlanguages.languages).toEqual([{
            name:'English',
            value: 'en'
        }]);
    });
    test("Default Test", () => {
        const newDefault = commonReducer(state, {type:'testDefaultCase',payload:{}});
        expect(newDefault).toEqual(state);
    });

    test('setGenresList test', () => {
        const genre = setGenresList({});
        expect(genre).toEqual({type:'SET_GENRES_LIST',payload:{}})
    });

    test('setLanguagesList test', () => {
        //@ts-ignore
        const lang = setLanguagesList({ name: '', value:''});
        expect(lang).toEqual({type:'SET_LANGUAGES_LIST',payload:{ name: '', value:''}});
    });
});