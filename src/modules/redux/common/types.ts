export interface ICommonReducer {
    genres: Array<{}>;
    languages: Languages,
}

export interface ICommonReducerSelector {
    commonReducer: {
        genres: Array<{id: number; name: string}>;
        languages: Array<{value:string; name: string;}>;
    }
}

export interface IStateCommonReducer {
    genres: Array<{id: number; name: string}>;
    languages: Array<{value:string; name: string;}>;
}

export type Languages = Array<{name: string; value: string}>