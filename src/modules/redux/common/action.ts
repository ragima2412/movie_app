import { types } from './actionTypes';
import { Languages } from './types';

export type ISetLanguagesList = {
    type: string;
    payload: Languages
}

export const setGenresList = (payload:object) => ({
    type: types.SET_GENRES_LIST,
    payload,
});

export const setLanguagesList = (payload: Languages): ISetLanguagesList  => ({
    type: types.SET_LANGUAGES_LIST,
    payload
});