import { types } from './actionTypes';
import { ICommonReducer, IStateCommonReducer } from './types';

const initialState = {
    genres: [],
    languages: [],
};

const commonReducer = (state: ICommonReducer = initialState, action: {type: string; payload: [{id: number, name?: string, value?: string}] }) => {
    switch (action.type) {
        case types.SET_GENRES_LIST:
            return {
                ...state,
                genres: action.payload
            };
        case types.SET_LANGUAGES_LIST:
            return {
                ...state,
                languages: action.payload
            };
        default: return state;
    }
};

export const selectGenres = (state: IStateCommonReducer) => state.genres;
export const selectLanguages = (state: IStateCommonReducer) => state.languages;

export default commonReducer;