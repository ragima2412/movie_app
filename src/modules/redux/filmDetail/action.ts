import { actionTypes } from './actionTypes';
import { IFilm, IPayload } from './types';

export const setFilmDetailsAC = (payload: IFilm) => ({
    type: actionTypes.SET_FILM_DETAILS,
    payload
});

export const setFilmGenresAC = (payload: IPayload) => ({
    type: actionTypes.SET_FILM_GENRES,
    payload
});

export const setAllFilmsLIstAC = (payload: Array<IFilm>) => ({
    type: actionTypes.SET_ALL_FILMS_LIST,
    payload
});
