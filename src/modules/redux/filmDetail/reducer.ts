import { actionTypes } from './actionTypes';
import { IPayload, IStateFilmDetail } from './types';

const initialState = {
   filmDetail:[],
   allFilmsList: []
};

const filmDetailReducer = (state = initialState, action: {type: string; payload: object}) => {
    switch (action.type) {
        case actionTypes.SET_FILM_DETAILS:
            return {
                ...state,
                filmDetail: action.payload
            };
        case actionTypes.SET_ALL_FILMS_LIST:
            return {
                ...state,
                allFilmsList: action.payload
            };              
        default: return state;
    }
};

export const getFilmDetail = (state: IStateFilmDetail) => state.filmDetail; 
export const getAllFilmList = (state: IStateFilmDetail) => state.allFilmsList; 

export default filmDetailReducer;
