import { setAllFilmsLIstAC, setFilmDetailsAC } from './action';
import filmDetailReducer from './reducer';

const state : {
    filmDetail:[],
    allFilmsList: []
} = {
    filmDetail:[],
    allFilmsList: []
}

describe('FilmDetailReducer test', () => {

    test('Film Detail test', () => {
        const newFilmDetail = (filmDetailReducer(state,setFilmDetailsAC({
            adult: false,
            backdrop_path: "backdrop_path",
            budget: 100,
            genre_ids: [1,2],
            homepage: 'homepage',
            id: 1,
            imdb_id: 'imdb_id',
            movie_rate: false,
            original_language: 'en',
            original_title: 'original_title',
            overview: 'overview',
            popularity: 2,
            poster_path: 'poster_path',
            release_date: 'release_date',
            revenue: 1,
            runtime: 1,
            status: 1,
            tagline: 'tagline',
            title: 'title',    
    })))
    expect(newFilmDetail.filmDetail).toEqual({
        adult: false,
            backdrop_path: "backdrop_path",
            budget: 100,
            genre_ids: [1,2],
            homepage: 'homepage',
            id: 1,
            imdb_id: 'imdb_id',
            movie_rate: false,
            original_language: 'en',
            original_title: 'original_title',
            overview: 'overview',
            popularity: 2,
            poster_path: 'poster_path',
            release_date: 'release_date',
            revenue: 1,
            runtime: 1,
            status: 1,
            tagline: 'tagline',
            title: 'title',   
    })
    })
    test('All FilmList test', () => {
        const newAllfilmList = (filmDetailReducer(state,setAllFilmsLIstAC({
            adult: false,
            backdrop_path: "backdrop_path",
            budget: 100,
            genre_ids: [1,2],
            homepage: 'homepage',
            id: 1,
            imdb_id: 'imdb_id',
            movie_rate: false,
            original_language: 'en',
            original_title: 'original_title',
            overview: 'overview',
            popularity: 2,
            poster_path: 'poster_path',
            release_date: 'release_date',
            revenue: 1,
            runtime: 1,
            status: 1,
            tagline: 'tagline',
            title: 'title',    
    })))
    expect(newAllfilmList.allFilmsList).toEqual({
        adult: false,
            backdrop_path: "backdrop_path",
            budget: 100,
            genre_ids: [1,2],
            homepage: 'homepage',
            id: 1,
            imdb_id: 'imdb_id',
            movie_rate: false,
            original_language: 'en',
            original_title: 'original_title',
            overview: 'overview',
            popularity: 2,
            poster_path: 'poster_path',
            release_date: 'release_date',
            revenue: 1,
            runtime: 1,
            status: 1,
            tagline: 'tagline',
            title: 'title',   
    })
    })
    test("Default Test", () => {
        const newDefault = filmDetailReducer(state, {type:'testDefaultCase',payload:{
            adult: false,
            backdrop_path: "backdrop_path",
            budget: 100,
            genre_ids: [1,2],
            homepage: 'homepage',
            id: 1,
            imdb_id: 'imdb_id',
            movie_rate: false,
            original_language: 'en',
            original_title: 'original_title',
            overview: 'overview',
            popularity: 2,
            poster_path: 'poster_path',
            release_date: 'release_date',
            revenue: 1,
            runtime: 1,
            status: 1,
            tagline: 'tagline',
            title: 'title',    
    }});
        expect(newDefault).toEqual(state);
    });
})
