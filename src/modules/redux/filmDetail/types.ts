export interface IFilmsReducer {
    filmsList: Array<IFilm>,
    totalCount: number,
    currentPage: number,
    isLoading: boolean,
    isFiltered: boolean,
    filteredData: Array<IFilm>
}

export interface IFilm {
    id: number,
    poster_path: string,
    genre_ids: Array<number>,
    title: string,
    release_date:string,
    popularity:number,
    tagline: string,
    overview: string,
    adult?: boolean,
    runtime: number,
    budget: number,
    revenue: number,
    homepage: string,
    backdrop_path: string
}

export interface IState {
    filmDetailReducer: {
        filmDetail: IFilm,
        allFilmsList: IFilm,        
    },
}

export interface IStateFilmDetail {
    filmDetail: IFilm,
    allFilmsList: IFilm,        
}

export interface IPayload {
        adult?: boolean,
        backdrop_path?: string,
        budget?: number,
        genre_ids: Array<number>,
        homepage?: string,
        id?: number,
        imdb_id?: string,
        movie_rate?: boolean,
        original_language?: string,
        original_title?: string,
        overview?: string,
        popularity?: number,
        poster_path: string,
        release_date?: string,
        revenue?: number,
        runtime?: number,
        status?: number,
        tagline?: string,
        title: string,    
}
