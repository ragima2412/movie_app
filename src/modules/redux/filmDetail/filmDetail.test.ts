import { setAllFilmsLIstAC, setFilmDetailsAC, setFilmGenresAC } from './action';
import filmDetailReducer from './reducer';
import { IPayload } from './types';
import { getAllFilmList, getFilmDetail } from './selectors';

describe('test filmDetailReducer', () => {
    
    let film : IPayload;

    const state: {
        filmDetailReducer:{
            filmDetail:[],
            allFilmsList: []
        }
    } = {
        filmDetailReducer:{
            filmDetail:[],
            allFilmsList: []
        }
    }

    beforeEach(() => {
        film = {
            adult: true,
            backdrop_path: 'backdrop_path',
            budget: 1,
            genre_ids: [1,2],
            homepage: 'homepage',
            id: 1,
            imdb_id: 'imdb_id',
            movie_rate: false,
            original_language: 'original_language',
            original_title: 'string',
            overview: 'string',
            popularity: 1,
            poster_path: 'string',
            release_date: 'string',
            revenue: 1,
            runtime: 1,
            status: 1,
            tagline: 'string',
            title: 'string',  
        }
    })
    test('set films list', () => {
        const films = filmDetailReducer(state.filmDetailReducer, setFilmDetailsAC(film))
        expect(films.filmDetail).toEqual({
            adult: true,
            backdrop_path: 'backdrop_path',
            budget: 1,
            genre_ids: [1,2],
            homepage: 'homepage',
            id: 1,
            imdb_id: 'imdb_id',
            movie_rate: false,
            original_language: 'original_language',
            original_title: 'string',
            overview: 'string',
            popularity: 1,
            poster_path: 'string',
            release_date: 'string',
            revenue: 1,
            runtime: 1,
            status: 1,
            tagline: 'string',
            title: 'string',
        })
    });
    test('set all films list', () => {
        const allFilmsList = filmDetailReducer(state.filmDetailReducer, setAllFilmsLIstAC(film))
        expect(allFilmsList.allFilmsList).toEqual({
            adult: true,
            backdrop_path: 'backdrop_path',
            budget: 1,
            genre_ids: [1,2],
            homepage: 'homepage',
            id: 1,
            imdb_id: 'imdb_id',
            movie_rate: false,
            original_language: 'original_language',
            original_title: 'string',
            overview: 'string',
            popularity: 1,
            poster_path: 'string',
            release_date: 'string',
            revenue: 1,
            runtime: 1,
            status: 1,
            tagline: 'string',
            title: 'string',
        })
    });
    test(' default case', () => {
        const defaultState = filmDetailReducer(state.filmDetailReducer, {type: "12", payload: film})
        expect(defaultState).toEqual( {filmDetail:[],
            allFilmsList: []})
    })
    test(' setFilmDetailsAC  test', () => {
        const filmDetail = setFilmDetailsAC(film);
        expect(filmDetail).toEqual({
            type: "SET_FILM_DETAILS",
            payload:   {adult: true,
            backdrop_path: 'backdrop_path',
            budget: 1,
            genre_ids: [1,2],
            homepage: 'homepage',
            id: 1,
            imdb_id: 'imdb_id',
            movie_rate: false,
            original_language: 'original_language',
            original_title: 'string',
            overview: 'string',
            popularity: 1,
            poster_path: 'string',
            release_date: 'string',
            revenue: 1,
            runtime: 1,
            status: 1,
            tagline: 'string',
            title: 'string',  }
        })
    })   
    test('setFilmGenresAC test', () => {
        const filmGenres = setFilmGenresAC(film);
        expect(filmGenres).toEqual({ type: "SET_FILM_GENRES",
            payload: film})
    }) 
    test('setAllFilmsLIstAC test', () => {
        const allFilms = setAllFilmsLIstAC(film);
        expect(allFilms). toEqual({ type: "SET_ALL_FILMS_LIST",
            payload:film});
    })
    test('getFilmDetail selector test', () => {
        //@ts-ignore
        const select = getFilmDetail(state);
      expect(select).toEqual(state.filmDetailReducer.filmDetail);
    })
    test('getAllFilmList selector test', () => {
          //@ts-ignore
        const select = getAllFilmList(state);
        expect(select).toEqual(state.filmDetailReducer.allFilmsList);
    })
})
