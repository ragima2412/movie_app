import { combineReducers } from 'redux';
import signInReducer, * as fromSignIn from './signIn/reducer';
import signUpReducer, * as fromSignUp from './signUp/reducer';
import sliderReducer, * as fromSlider from './home/reducer';
import filmsReducer, * as fromFilms from './filmsList/reducer';
import commonReducer, * as fromCommon from './common/reducer';
import filmDetailReducer, * as fromFilmDetail from './filmDetail/reducer';
import filtersReducer, * as fromFilters from './filters/reducer';
import { ApplicationState } from './index';

export const REDUCER = Object.freeze({
    SIGN_IN: 'signIn',
    SIGN_UP: 'signUp',
    SLIDER: 'slider',
    FILMS: 'films',
    COMMON: 'common',
    FILM_DETAIL: 'filmDetail',
    FILTERS: 'filters'
});

export const rootReducer = combineReducers({
    [REDUCER.SIGN_IN]: signInReducer,
    [REDUCER.SIGN_UP]: signUpReducer,
    [REDUCER.SLIDER]: sliderReducer,
    [REDUCER.FILMS]: filmsReducer,
    [REDUCER.COMMON]: commonReducer,
    [REDUCER.FILM_DETAIL]: filmDetailReducer,
    [REDUCER.FILTERS]: filtersReducer
});

// region signInReducer
export const getUserData = (state: ApplicationState) => fromSignIn.getUserData(state[REDUCER.SIGN_IN]);
export const getUserProfile = (state: ApplicationState) => fromSignIn.getUserProfile(state[REDUCER.SIGN_IN]);
export const getModalWindow = (state: ApplicationState) => fromSignIn.getModalWindow(state[REDUCER.SIGN_IN]);
export const selectLogOut = (state: ApplicationState): boolean => fromSignIn.selectLogOut(state[REDUCER.SIGN_IN]);
export const setActiveButton = (state: ApplicationState) => fromSignIn.setActiveButton(state[REDUCER.SIGN_IN]);
export const isLoadingMessageSignIn = (state: ApplicationState) => fromSignIn.isLoadingMessageSignIn(state[REDUCER.SIGN_IN]);
// endregion

// region signUpReducer
export const messageSelector = (state: ApplicationState) =>  fromSignUp.messageSelector(state[REDUCER.SIGN_UP]);
export const modalSelector = (state: ApplicationState):boolean => fromSignUp.modalSelector(state[REDUCER.SIGN_UP]);
export const isLoadingMessage = (state: ApplicationState) => fromSignUp.isLoadingMessage(state[REDUCER.SIGN_UP]);
export const isUserInfo = (state: ApplicationState) => fromSignUp.isUserInfo(state[REDUCER.SIGN_UP]);
// endregion

// region slider
export const selectPopular = (state: ApplicationState) => fromSlider.selectPopular(state[REDUCER.SLIDER]);
export const selectNewReleases = (state: ApplicationState) => fromSlider.selectNewReleases(state[REDUCER.SLIDER]);
export const selectRandom = (state: ApplicationState) => fromSlider.selectRandom(state[REDUCER.SLIDER]);
export const selectIsLoading = (state: ApplicationState) => fromSlider.selectIsLoading(state[REDUCER.SLIDER]);
export const selectHomeLink = (state: ApplicationState) => fromSlider.selectHomeLink(state[REDUCER.SLIDER]);
export const selectMoviesLink = (state: ApplicationState) => fromSlider.selectMoviesLink(state[REDUCER.SLIDER]);
// endregion

//region filters 
export const selectLanguage = (state: ApplicationState) => fromFilters.selectLanguage(state[REDUCER.FILTERS]);
export const selectBudget = (state: ApplicationState) => fromFilters.selectBudget(state[REDUCER.FILTERS]);
export const selectPopularFilter = (state: ApplicationState) => fromFilters.selectPopularFilter(state[REDUCER.FILTERS]);
export const selectRevenue = (state: ApplicationState) => fromFilters.selectRevenue(state[REDUCER.FILTERS]);
export const selectSearch = (state: ApplicationState) => fromFilters.selectSearch(state[REDUCER.FILTERS]);
export const selectYear = (state: ApplicationState) => fromFilters.selectYear(state[REDUCER.FILTERS]);
// endregion

//region films
export const selectFilms = (state: ApplicationState) => fromFilms.selectFilms(state[REDUCER.FILMS]);
export const selectTotalCount = (state: ApplicationState) => fromFilms.selectTotalCount(state[REDUCER.FILMS]);
export const selectCurrentPage = (state: ApplicationState) => fromFilms.selectCurrentPage(state[REDUCER.FILMS]);
export const selectIsLoadingFilms = (state: ApplicationState) => fromFilms.selectIsLoadingFilms(state[REDUCER.FILMS]);
export const selectIsFiltered = (state: ApplicationState) => fromFilms.selectIsFiltered(state[REDUCER.FILMS]);
export const selectFilteredData = (state: ApplicationState) => fromFilms.selectFilteredData(state[REDUCER.FILMS]);
// endregion

//region common
export const selectGenres = (state: ApplicationState) => fromCommon.selectGenres(state[REDUCER.COMMON]);
export const selectLanguages = (state: ApplicationState) => fromCommon.selectLanguages(state[REDUCER.COMMON]);
// endregion

//region filmDetail
export const getFilmDetail = (state: ApplicationState) => fromFilmDetail.getFilmDetail(state[REDUCER.FILM_DETAIL]); 
export const getAllFilmList = (state: ApplicationState) => fromFilmDetail.getAllFilmList(state[REDUCER.FILM_DETAIL]);
// endregion