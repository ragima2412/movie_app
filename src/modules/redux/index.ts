import createSagaMiddleware from 'redux-saga';
import { createStore, applyMiddleware, Middleware, StoreEnhancer } from 'redux';
import rootSaga from '../saga';
import signInReducer from './signIn/reducer';
import signUpReducer from './signUp/reducer';
import commonReducer from './common/reducer';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import filmsReducer from './filmsList/reducer';
import  filmDetailReducer from './filmDetail/reducer';
import { rootReducer } from './rootReducer';
import { composeWithDevTools } from 'redux-devtools-extension';


const persistConfig = {
    key: 'signInReducer',
    whitelist: ['signInReducer','filmDetailReducer','commonReducer'],
    blacklist: ['signUpReducer', 'popularSliderReducer', 'filmsReducer'],
    storage,
};

const bindMiddleware = (middleware: Middleware): StoreEnhancer => {
    if (process.env.NODE_ENV === 'production') return applyMiddleware(middleware);
    return composeWithDevTools(applyMiddleware(middleware));
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const sagaMiddleware = createSagaMiddleware();

export const store = createStore(persistedReducer, bindMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga);

export const persistedStore = persistStore(store);

export type ApplicationState = ReturnType<typeof rootReducer>;