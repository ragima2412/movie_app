import { takeEvery, call, put } from 'redux-saga/effects';
import { setNewReleasesList, setPopularList, setRandomList } from '../../redux/home/action';
import { setGenresList } from '../../redux/common/action';
import { actionTypes } from './actionTypes';
import { apiRoutes, requestFunction } from '../../../services/apiRoutes';
import { setIsLoadingAC } from '../../redux/home/action';
import { Utils } from "../../../services/utils/index";
import { SagaIterator } from '@redux-saga/types';
import { setNewReleases, setPopular } from './action';
import { IFilm } from '../../redux/filmDetail/types';

export function* getRandomWorker(): SagaIterator {
    const randomFilms: Array<IFilm> = [];
    try {
        const result = yield call(requestFunction,`${apiRoutes.movie}`, 'GET');
        const totalCount = result?.totalCount;
        const randomfilmsIds: Array<number> = yield call(Utils.randomNums, 5, totalCount);
        for (let i = 1; i < randomfilmsIds.length; i++) {
            const result = yield call(requestFunction,`${apiRoutes.movie}/${randomfilmsIds[i]}`, 'GET');
            randomFilms.push(result.movie);
        }
        yield put(setRandomList(randomFilms));
        yield put(setIsLoadingAC(false));
    } catch (error) {
        console.warn("getRandomWorker ===>", error);
    }
}

export function* getRandomWatcher () {
    yield takeEvery(actionTypes.SET_RANDOM, getRandomWorker);
}

export function* getNewReleasesWorker({payload}:ReturnType<typeof setNewReleases>): SagaIterator { 
    try {
        yield put(setIsLoadingAC(true));
        const minusYears: Date = yield call(Utils.minusYears, new Date(), 2);
        const realeaseFirst: string = yield call(Utils.formatDate, minusYears);
        const releaseLast: string = yield call(Utils.formatDate, new Date());
        const url = `${apiRoutes.movie}?${apiRoutes.page}=${payload.page}&${apiRoutes.release_date_first}=${realeaseFirst}&${apiRoutes.release_date_last}=${releaseLast}`;
        const result = yield call(requestFunction, url, 'GET');
        result?.movies.sort((a: IFilm, b: IFilm) => +new Date(b.release_date) - +new Date(a.release_date));
        yield put(setNewReleasesList(result?.movies));
    } catch (error) {
        console.warn("getNewReleasesWorker ===>", error);
    }
}

export function* getNewReleasesWatcher () {
    yield takeEvery(actionTypes.SET_NEW_RELEASES, getNewReleasesWorker);
}

export function* getPopularWorker({payload}:ReturnType<typeof setPopular>): SagaIterator {
    try {
        const result = yield call(requestFunction,`${apiRoutes.movie}?${apiRoutes.page}=${payload.page}&${apiRoutes.popularity_min}=${payload.popularity}`, 'GET');
        result?.movies.sort((a: IFilm, b: IFilm) => b.popularity - a.popularity);
        yield put(setPopularList(result?.movies));
    } catch (error) {
        console.warn("getPopularWorker ===>", error);
    }
}

export function* getPopularWatcher () {
    yield takeEvery(actionTypes.SET_POPULAR, getPopularWorker);
}

export function* getGenresWorker(): SagaIterator {
    try {
        const result = yield call(requestFunction, apiRoutes.genres, 'GET');
        yield put(setGenresList(result?.genres));
    } catch (error) {
        console.warn("getGenresWorker ===>", error);
    }
}

export function* getGenresWatcher () {
    yield takeEvery(actionTypes.SET_GENRES, getGenresWorker);
}