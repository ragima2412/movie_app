import { expectSaga } from 'redux-saga-test-plan';
import { call } from 'redux-saga/effects'
import sliderReducer from 'modules/redux/home/reducer'
import { getGenresWorker, getNewReleasesWorker, getPopularWorker, getRandomWorker } from 'modules/saga/home/index';
import { setIsLoadingAC, setNewReleasesList, setPopularList, setRandomList } from 'modules/redux/home/action';
import { apiRoutes, requestFunction } from 'services/apiRoutes';
import { Utils } from 'services/utils';
import { setGenresList } from 'modules/redux/common/action';
import { setGenres, setNewReleases, setPopular, setRandom } from './action';

beforeAll(() => {
    jest.useFakeTimers('modern');
    jest.setSystemTime(new Date(2021, 12, 1));
})
    
afterAll(() => {
    jest.useRealTimers();
})

describe('home saga test', () => {
    const state = {
        popularList: [],
        newReleasesList: [],
        randomList: [],
    }

    it('getPopularWorker test', async () => {
        const resolve = {
            movies: [{
                popularity: 200,
                poster_path: 'poster_path',
                genre_ids: [1,2],
                title: 'title',
            }]
        }
        const payload = {
            page: 1,
            popularity: 200
        }
        const saga = expectSaga(getPopularWorker, {
            type: '',
            payload
        })
        .provide([
            [call(requestFunction,`${apiRoutes.movie}?${apiRoutes.page}=${payload.page}&${apiRoutes.popularity_min}=${payload.popularity}`, 'GET'), resolve] 
            //@ts-ignore
        ]).withReducer(sliderReducer, state)
       
        const result = await saga
        .call(requestFunction, `${apiRoutes.movie}?${apiRoutes.page}=${payload.page}&${apiRoutes.popularity_min}=${payload.popularity}`, 'GET')
        .put(setPopularList(resolve.movies))
        .run()
        expect(result.effects).toEqual({})
    })
    
    it('setPopular action test', () => {
        //@ts-ignore
        const popular = setPopular()
        expect(popular).toEqual({type: 'SET_POPULAR'})
    });

    it('getNewReleasesWorker test', async () => {
        const resolve = {
            movies: [{
                release_date: '',
                poster_path: 'poster_path',
                genre_ids: [1,2],
                title: 'title',
            }]
        }
        const payload = {
            page: 1
        }
        const minusYears = {}
        const realeaseFirst = ''
        const releaseLast = ''
        const saga = expectSaga(getNewReleasesWorker, {
            type: '',
            payload
        })
        .provide([
            [call(Utils.minusYears, new Date(), 2), {}],
            [call(Utils.formatDate, minusYears), {}],
            [call(Utils.formatDate, new Date()), {}],
            [call(requestFunction,`${apiRoutes.movie}?${apiRoutes.page}=${payload.page}&${apiRoutes.release_date_first}=${realeaseFirst}&${apiRoutes.release_date_last}=${releaseLast}`, 'GET'), resolve] 
            //@ts-ignore
        ]).withReducer(sliderReducer, state)
       
        const result = await saga
        .put(setIsLoadingAC(true))
        .call(Utils.minusYears, new Date(), 2)
        .call(Utils.formatDate, minusYears)
        .call(Utils.formatDate, new Date())
        .call(requestFunction, `${apiRoutes.movie}?${apiRoutes.page}=${payload.page}&${apiRoutes.release_date_first}=${realeaseFirst}&${apiRoutes.release_date_last}=${releaseLast}`, 'GET')
        .put(setNewReleasesList(resolve.movies))
        .put(setIsLoadingAC(false))
        .run()
        expect(result.effects).toEqual({})
    })

    it('setNewReleases action test', () => {
        //@ts-ignore
        const newReleases = setNewReleases()
        expect(newReleases).toEqual({type: 'SET_NEW_RELEASES'})
    });

    it('getRandomWorker test', async () => {
        const resolve = {
            totalCount: 100,
            movie: {
                poster_path: 'poster_path',
                genre_ids: [1,2],
                title: 'title',
            }
        }
        const resolveRandomFilms = {
            movie: {
                poster_path: 'poster_path',
                genre_ids: [1,2],
                title: 'title',
            }
        }
        //@ts-ignore
        let randomFilms = [{
            poster_path: 'poster_path',
            genre_ids: [1,2],
            title: 'title',
        }];
        const randomfilmsIds = [1, 2]
        const payload = {
            page: 1
        }
        const totalCount = 100
        const saga = expectSaga(getRandomWorker, {
            type: '',
            payload
        })
        .provide([
            [call(requestFunction,`${apiRoutes.movie}?${apiRoutes.page}=${payload.page}`, 'GET'), resolve],
            [call(Utils.randomNums, 5, totalCount), [1, 2]],
            [call(requestFunction,`${apiRoutes.movie}/${randomfilmsIds[1]}`, 'GET'), resolveRandomFilms]
            //@ts-ignore
        ]).withReducer(sliderReducer, state)
       
        const result = await saga
        .call(requestFunction, `${apiRoutes.movie}?${apiRoutes.page}=${payload.page}`, 'GET')
        .call(Utils.randomNums, 5, totalCount)
        .call(requestFunction,`${apiRoutes.movie}/${randomfilmsIds[1]}`, 'GET')
        .put(setRandomList(randomFilms))
        .run()
        expect(result.effects).toEqual({})
    })

    it('setRandom action test', () => {
        //@ts-ignore
        const random = setRandom()
        expect(random).toEqual({type: 'SET_RANDOM'})
    });

    it('getGenresWorker test', async () => {
        const resolve = {
            genres: [{
                id: 1,
                name: '',
            }]
        }
        const saga = expectSaga(getGenresWorker)
        .provide([
            [call(requestFunction, apiRoutes.genres, 'GET'), resolve] 
            //@ts-ignore
        ]).withReducer(sliderReducer, state)
       
        const result = await saga
        .call(requestFunction, apiRoutes.genres, 'GET')
        .put(setGenresList(resolve.genres))
        .run()
        expect(result.effects).toEqual({})
    })
    
    it('setGenres action test', () => {
        const genres = setGenres()
        expect(genres).toEqual({type: 'SET_GENRES'})
    });
})