import { actionTypes } from './actionTypes';

export const setPopular = (payload: {page: number, popularity: number}): {type: actionTypes.SET_POPULAR,payload:{page: number, popularity: number}} => ({
    type: actionTypes.SET_POPULAR,
    payload
});

export const setNewReleases = (payload: {page: number}): { type: actionTypes.SET_NEW_RELEASES, payload: {page: number}} => ({
    type: actionTypes.SET_NEW_RELEASES,
    payload
});

export const setRandom= (): {type:actionTypes.SET_RANDOM} => ({
    type: actionTypes.SET_RANDOM,
});

export const setGenres = (): {type:actionTypes.SET_GENRES} => ({
    type: actionTypes.SET_GENRES
});
