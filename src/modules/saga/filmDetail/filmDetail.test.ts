import { setFilmDetailsAC } from './../../redux/filmDetail/action';
import { getAllFilmList } from './../../redux/filmDetail/selectors';
import { select, call, all } from 'redux-saga/effects';
import { apiRoutes, requestFunction } from './../../../services/apiRoutes/index';
import { expectSaga } from 'redux-saga-test-plan';
import { setAllFilmsWorker, setFilmDetailsWorker } from '.';
import { setAllFilmsAC, setFilmDetailAC } from './action';
import { IFilm, IPayload } from '../../redux/filmDetail/types';

describe('filmDetails saga',() => {
   let state = {
        filmDetailReducer:{
            filmDetail:[],
            allFilmsList: []
        }
    }
    
    let payload: IPayload;
    let film: IFilm;
    
    beforeEach(() => {
        film ={
            id: 2,
            poster_path: "string",
            genre_ids: [1,2,3],
            title: "string",
            release_date:"string",
            popularity:12,
            tagline: "string",
            overview: "string",
            adult: true,
            runtime: 2,
            budget: 4,
            revenue: 12,
            homepage: "string",
            backdrop_path: "string"
       },
        payload = {
        adult: true,
        backdrop_path: 'string',
        budget: 1,
        genre_ids: [],
        homepage: 'string',
        id: 1,
        imdb_id: 'string',
        movie_rate: false,
        original_language: 'string',
        original_title: 'string',
        overview: 'string',
        popularity: 123,
        poster_path: 'string',
        release_date: 'string',
        revenue: 1,
        runtime: 3,
        status: 4,
        tagline: 'string',
        title: 'string'
     }
   })
     test('setFilmDetailsWorker test with payload Id', async () => {

         const saga = expectSaga(setFilmDetailsWorker, {
            //@ts-ignore
          type: 'SET_FILM_DETAILS',
          payload: film
         })
              .provide([
               [select(getAllFilmList), [film]]
              ]).withState(state)  
              const result = await saga
              .select(getAllFilmList)
              .put(setFilmDetailsAC(film))
              .run()
              expect(result.effects).toEqual({})             
     }) 
test('setAllFilmsWorker test', async() => {
    let film = [{movies: [{

        id: 2,
        poster_path: "string",
        genre_ids: [1,2,3],
        title: "string",
        release_date:"string",
        popularity:12,
        tagline: "string",
        overview: "string",
        adult: true,
        runtime: 2,
        budget: 4,
        revenue: 12,
        homepage: "string",
        backdrop_path: "string"
    }]
   }]; 
    const pages: Array<number> = [1,2,3,4,5];
    const saga = expectSaga(setAllFilmsWorker)
    .provide([
      [all([call(requestFunction, `${apiRoutes.movie}?page=${pages[0]}`, 'GET')]),film]   
    ]).withState(state)
    const result = await saga
    .call(requestFunction, `${apiRoutes.movie}?page=${pages[0]}`, 'GET')
    .call(requestFunction, `${apiRoutes.movie}?page=${pages[1]}`, 'GET')
    .call(requestFunction, `${apiRoutes.movie}?page=${pages[2]}`, 'GET')
    .call(requestFunction, `${apiRoutes.movie}?page=${pages[3]}`, 'GET')
    .call(requestFunction, `${apiRoutes.movie}?page=${pages[4]}`, 'GET')                   
        .run()
        expect(result.effects).toEqual({})
})
    test('setAllFilmDetailAC test', () => {     
        const allFilms = setAllFilmsAC();
        expect(allFilms).toEqual({type: 'SET_ALL_FILMS'})
    }) 
    test('setFilmDetailAC test', () => {           
        const filmDetail = setFilmDetailAC(film);
        expect(filmDetail).toEqual({type: 'SET_FILM_DETAIL',
           payload:film})
    }) 
}) 
