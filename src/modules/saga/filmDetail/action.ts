import { IFilm } from '../../redux/filmDetail/types';
import { Action } from 'redux';
import { actionTypes } from './actionTypes';

export const setFilmDetailAC = (payload:IFilm) : {type:actionTypes.SET_FILM_DETAIL, payload: IFilm} => ({
    type: actionTypes.SET_FILM_DETAIL,
    payload
});

export const setAllFilmsAC = (): Action<actionTypes.SET_ALL_FILMS> => ({
    type: actionTypes.SET_ALL_FILMS
});
