import { IFilm } from 'modules/redux/filmDetail/types';
import { takeEvery, put, select, call, all } from 'redux-saga/effects';
import {  setAllFilmsLIstAC, setFilmDetailsAC } from '../../redux/filmDetail/action';
import { actionTypes } from './actionTypes';
import { requestFunction } from '../../../services/apiRoutes';
import { getAllFilmList } from '../../redux/rootReducer';
import { SagaIterator } from '@redux-saga/types';
import { setFilmDetailAC } from './action';


export function* setFilmDetailsWatcher(): SagaIterator {
    yield takeEvery(actionTypes.SET_FILM_DETAIL, setFilmDetailsWorker);
}

export function* setFilmDetailsWorker({ payload }: ReturnType<typeof setFilmDetailAC>): SagaIterator {
    try {
        const filmsList = yield select(getAllFilmList);
        if (payload.id) {
            const chosenFilm: IFilm = filmsList.find((item: IFilm) => item.id === payload.id);
            yield put(setFilmDetailsAC(chosenFilm));
        }
    } catch (error) {
        console.warn(error);
    }
}

export function* setAllFilmsWatcher(): SagaIterator {
    yield takeEvery(actionTypes.SET_ALL_FILMS, setAllFilmsWorker);
}

export function* setAllFilmsWorker(): SagaIterator {
    try {
        const data: Array<IFilm> = [];
        const pages: Array<number> = [1,2,3,4,5];
        const result: Array<{movies:Array<IFilm>}> = yield all(pages.map((item: number) => call(requestFunction, `movie?page=${item}`, 'GET')));
        result.map(item => data.push(...item.movies));  
        yield put(setAllFilmsLIstAC(data));
    } catch (error) {
        console.warn('setAllFilmsWorker ===>', error);
    }
}

