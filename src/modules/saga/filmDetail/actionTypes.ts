export enum actionTypes {
  SET_FILM_DETAIL = 'SET_FILM_DETAIL',
  SET_ALL_FILMS = 'SET_ALL_FILMS'
}
