export enum actionTypesSignInSaga {
    SIGN_IN = 'SIGN_IN',
    CHECK_INPUT_DATA = 'CHECK_INPUT_DATA'
}