import { SagaIterator } from '@redux-saga/types';
import { takeEvery, call, put } from 'redux-saga/effects';
import { apiRoutes, requestFunction } from '../../../services/apiRoutes';
import { setTokenAC, setUserIdAC, setMessageSignIn, setModalSignIn } from '../../redux/signIn/action';
import { signInAC } from './action';
import { actionTypesSignInSaga } from './actionTypes';

export function* signInWorker({ payload }: ReturnType<typeof signInAC>) {
    try {  
        const authData: {token: string, login: string, password: string, message: string, userId: number} = yield call(requestFunction,apiRoutes.sign_in,'POST',payload);
        if(authData.password || authData.login || authData.message){
            yield put(setMessageSignIn(authData));
        } else {
            yield put(setTokenAC(authData.token));
            yield put(setUserIdAC(authData.userId));
            yield put(setModalSignIn(false));
        }
    }
    catch (error) {
        console.warn(error);
    }
}

export function* signInWatcher(): SagaIterator {
    yield takeEvery(actionTypesSignInSaga.SIGN_IN, signInWorker);
}
