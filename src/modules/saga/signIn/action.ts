import { actionTypesSignInSaga } from './actionTypes';


export const signInAC = (payload: {login:string, password:string}): {type: actionTypesSignInSaga.SIGN_IN, payload : {login:string, password:string}} => ({
    type: actionTypesSignInSaga.SIGN_IN,
    payload
});