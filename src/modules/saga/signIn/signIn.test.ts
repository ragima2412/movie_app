//@ts-ignore
import { setMessageAC, setMessageSignIn, setModalSignIn, setTokenAC, setUserIdAC } from 'modules/redux/signIn/action'
import signInReducer from 'modules/redux/signIn/reducer'
import { expectSaga } from 'redux-saga-test-plan'
import { call } from 'redux-saga/effects'
import { apiRoutes, requestFunction } from 'services/apiRoutes'
import { signInWorker } from '.'

const signIn = {
    login: '',
    token: '',
    message: {
        login:'',
        password:'',
        message:'',
    },
    userId: '',
    password: '',
    modalWindow: false,
    logOut: false,
    activeButton: false,
    isLoadingMessage: false
};

describe('Sign In Saga Test', () => {

    test('signInWorker test with token', async () => {
        const payload = {
            login: '',
            password: '',
        };
        const authData = {
            token: "", 
            login: "",
            password: "",
            message: "",
            userId: 1
        };
        const saga = expectSaga(signInWorker, {
            //@ts-ignore
            type: '',
            payload
        })
            .provide([
                [call(requestFunction, apiRoutes.sign_in, 'POST', payload), {token: "token", userId:1}],
                //@ts-ignore
            ]).withReducer(signInReducer, signIn);

        const result = await saga
            .call(requestFunction, apiRoutes.sign_in, 'POST', payload)
            .put(setTokenAC('token'))
            .put(setUserIdAC(1))
            .put(setModalSignIn(false))
            .run();

        expect(result.effects).toEqual({});
    });

    test('signInWorker test with error password', async () => {
        const payload = {
            login: '',
            password: '',
        };
        const saga = expectSaga(signInWorker, {
            //@ts-ignore
            type: '',
            payload
        })
            .provide([
                [call(requestFunction, apiRoutes.sign_in, 'POST', payload), { login: '', password: 'error', message: '' }],
                //@ts-ignore
            ]).withReducer(signInReducer, signIn);
        const result = await saga
            .call(requestFunction, apiRoutes.sign_in, 'POST', payload)
            .put(setMessageSignIn({ login: '', password: 'error', message: '' }))
            .run();

        expect(result.effects).toEqual({});
    });

    test('signInWorker with error login', async () => {
        const payload = {
            login: '',
            password: ''
        };
        const saga = expectSaga(signInWorker, {
            //@ts-ignore
            type: '',
            payload
        })
            .provide([
                [call(requestFunction, apiRoutes.sign_in, 'POST', payload), { login: 'error', password: '', message: '' }],
                //@ts-ignore
            ]).withReducer(signInReducer, signIn);
        const result = await saga
            .call(requestFunction, apiRoutes.sign_in, 'POST', payload)
            .put(setMessageSignIn({ login: 'error', password: '', message: '' }))
            .run();
            expect(result.effects).toEqual({});
    });

    test('signInWorker with error message', async () => {
        const payload = {
            login: '',
            password: ''
        };
        const saga = expectSaga(signInWorker, {
            //@ts-ignore
            type: '',
            payload
        })
        .provide([
            [call(requestFunction, apiRoutes.sign_in, 'POST', payload), { login: '', password: '', message: 'error' }],
            //@ts-ignore
        ]).withReducer(signInReducer, signIn);
        const result = await saga
        .call(requestFunction, apiRoutes.sign_in, 'POST', payload)
        .put(setMessageSignIn({ login: '', password: '', message: 'error'}))
        .run();
        expect(result.effects).toEqual({});
    });
});
