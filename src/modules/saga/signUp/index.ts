import { takeEvery, call, put } from 'redux-saga/effects';
import { setLoginAC, setModalSignIn } from '../../redux/signIn/action';
import { setMessageAC, setModalSignUp } from '../../redux/signUp/action';
import { signInAC } from '../signIn/action';
import { actionTypesSingUpSaga } from './actionTypes';
import { apiRoutes, requestFunction } from '../../../services/apiRoutes';
import { signUpAC } from './action';

export function* signUpWorker({ payload }: ReturnType<typeof signUpAC>) {
    try {
        const result: { login: string, password: string, message: string, first_name: string, last_name: string } = yield call(requestFunction, apiRoutes.sign_up, 'POST', payload);
        yield put(setMessageAC(result));
        if (result.message == 'Registration successful') {
            yield put(signInAC({ login: payload.login, password: payload.password }));
            yield put(setLoginAC(payload.login));
            yield put(setModalSignIn(false));
            yield put(setModalSignUp(false));
        }
    }
    catch (error) {
        console.warn(error);
    }
}

export function* signUpWatcher() {
    yield takeEvery(actionTypesSingUpSaga.SET_AUTH, signUpWorker);
}