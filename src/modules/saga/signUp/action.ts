import { actionTypesSingUpSaga } from './actionTypes';

export const signUpAC = (payload:{ first_name: string, last_name: string, login: string, password: string }) : {type: actionTypesSingUpSaga.SET_AUTH, payload: { first_name: string, last_name: string, login: string, password: string }} => ({
    type: actionTypesSingUpSaga.SET_AUTH,
    payload
});

export const setEmptyMessageAC = () => ({
    type: actionTypesSingUpSaga.SET_EMPTY_MESSAGE,
});