import { setLoginAC, setModalSignIn } from 'modules/redux/signIn/action';
import { setMessageAC, setModalSignUp } from 'modules/redux/signUp/action';
import signUpReducer from 'modules/redux/signUp/reducer';
import { expectSaga } from 'redux-saga-test-plan';
import { call } from 'redux-saga/effects';
import { apiRoutes, requestFunction } from 'services/apiRoutes';
import { signUpWorker } from '.';
import { signInAC } from '../signIn/action';
import { setEmptyMessageAC, signUpAC } from './action';

const signUp = {
    message: {
        first_name: '',
        last_name: '',
        login: '',
        password: '',
        message: ''
    }
};

describe('Sign Up Saga Test', () => {
    test('SignUpWorker test registration successfull', async () => {
        const payload = {
            login: '',
            password: '',
            first_name: '',
            last_name: ''
        };
        const response = {
            message: 'Registration successful'
        };
        const saga = expectSaga(signUpWorker, {
            //@ts-ignore
            type: '',
            payload
        })
            .provide([
                [call(requestFunction, apiRoutes.sign_up, 'POST', payload), response]
                //@ts-ignore
            ]).withReducer(signUpReducer, signUp);
        const result = await saga
            .call(requestFunction, apiRoutes.sign_up, 'POST', payload)
            //@ts-ignore
            .put(setMessageAC(response))
            .put(signInAC({ login: payload.login, password: payload.password }))
            .put(setLoginAC(payload.login))
            .put(setModalSignIn(false))
            .put(setModalSignUp(false))
            .run();
        expect(result.effects).toEqual({});
    });

    test('SignUpWorker test with error login', async () => {
        const payload = {
            login: '',
            password: '',
            first_name: '',
            last_name: ''
        };
        const saga = expectSaga(signUpWorker, {
            //@ts-ignore
            type: '',
            payload
        })
            .provide([
                [call(requestFunction, apiRoutes.sign_up, 'POST', payload), {login: '', password: '', message: ''}]
                //@ts-ignore
            ]).withReducer(signUpReducer, signUp);
        const result = await saga
            .call(requestFunction, apiRoutes.sign_up, 'POST', payload)
            .put(setMessageAC({login: '', password: '', message: ''}))
            .run();
            expect(result.effects).toEqual({});
    });

    test('signUpAC test', () => {
        const signUP = signUpAC({first_name: '', last_name: '', login: '', password: ''});
        expect(signUP).toEqual({type: 'SET_AUTH', payload: {first_name: '', last_name: '', login: '', password: ''}});
    });

    test('setEmptyMessageAC test', () => {
        const signUP = setEmptyMessageAC();
        expect(signUP).toEqual({type: 'SET_EMPTY_MESSAGE'});
    });
});