import { spawn } from '@redux-saga/core/effects';
import { signInWatcher } from './signIn';
import { signUpWatcher } from './signUp';
import { filterWatcher, languagesWatcher, searchWatcher } from './filters';
import { getGenresWatcher, getNewReleasesWatcher, getPopularWatcher, getRandomWatcher } from './home';
import { setAllFilmsWatcher, setFilmDetailsWatcher } from './filmDetail';
import { getFilmsFirstWatcher, getFilmsWatcher } from './filmsList';

export default function* rootSaga() {
    yield spawn(signUpWatcher);
    yield spawn(signInWatcher);
    yield spawn(languagesWatcher);
    yield spawn(filterWatcher);
    yield spawn(searchWatcher);
    yield spawn(getFilmsWatcher);
    yield spawn(getGenresWatcher);
    yield spawn(getPopularWatcher);
    yield spawn(getNewReleasesWatcher);
    yield spawn(getRandomWatcher);
    yield spawn(setFilmDetailsWatcher);
    yield spawn(setAllFilmsWatcher);
    yield spawn(getFilmsFirstWatcher);
}