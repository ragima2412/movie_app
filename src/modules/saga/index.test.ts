import rootSaga from '../saga/index';
import SagaTester from 'redux-saga-tester';
import {rootReducer} from '../redux/index';
import { signUpAC } from './signUp/action';
import { signInAC } from './signIn/action';
import { getAllFilms, getLanguages, getSearch } from './filters/action';
import { setFilms, setFirstFilms } from './filmsList/action';
import { setGenres, setNewReleases, setPopular, setRandom } from './home/action';
import { setAllFilmsAC, setFilmDetailAC } from './filmDetail/action';

describe ('root saga', () => {
    const film = {
        id: 1,
        poster_path: "string",
        genre_ids: [1,2],
        title: "string",
        release_date:"string",
        popularity:4,
        tagline: "string",
        overview: "string",
        adult: true,
        runtime: 1,
        budget: 1,
        revenue: 1,
        homepage: "string",
        backdrop_path: "string"
    }
    test('root saga test', async () => {
       const saga = new SagaTester({
        rootReducer
       }); 
       saga.start(rootSaga);
       saga.dispatch(signUpAC({ first_name: "string", last_name: "string", login: "string", password: "string" }));
       saga.dispatch(signInAC({login:"string", password:"string"}));
       saga.dispatch(getLanguages());
       saga.dispatch(getAllFilms({
        budget: [""],
        value: "string",
        popular:[""],
        minYear: "string",
        maxYear: "string",
        revenue: [""]
    }));
    saga.dispatch(getSearch(""));
    saga.dispatch(setFilms(1));
    saga.dispatch(setGenres());
    saga.dispatch(setPopular({page:2, popularity: 2}));
    saga.dispatch(setNewReleases({page: 4}));
    saga.dispatch(setRandom());
    saga.dispatch(setFilmDetailAC(film));
    saga.dispatch(setAllFilmsAC());
    saga.dispatch(setFirstFilms(2));
    });
})