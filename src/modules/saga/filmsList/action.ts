import { actionTypes } from './actionTypes';

export const setFilms = (payload: number) : { type: actionTypes.SET_FILMS, payload: number} => ({
    type: actionTypes.SET_FILMS,
    payload
});

export const setGenres = () : {type: actionTypes.SET_GENRES} => ({
    type: actionTypes.SET_GENRES
});

export const setFirstFilms = (payload: number) : {type: actionTypes.SET_FIRST_FILMS,payload: number} => ({
    type: actionTypes.SET_FIRST_FILMS,
    payload
});
