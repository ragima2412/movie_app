import { takeEvery, call, put } from 'redux-saga/effects';
import { setFilmsList, setIsLoadingAC } from '../../redux/filmsList/action';
import { actionTypes } from './actionTypes';
import { setGenresList } from '../../redux/common/action';
import { apiRoutes, requestFunction } from '../../../services/apiRoutes';
import { SagaIterator } from '@redux-saga/types';
import { setFilms, setFirstFilms } from './action';

export function* getFilmsWorker({payload}: ReturnType<typeof setFilms>): SagaIterator {
    try {
        const result = yield call(requestFunction, `movie?page=${payload}`, 'GET');
        yield put(setFilmsList(result));
    } catch (error) {
        console.warn("getFilmsWorker ===>", error);
    }
}

export function* getFilmsWatcher(): SagaIterator {
    yield takeEvery(actionTypes.SET_FILMS, getFilmsWorker);
}

export function* getGenresWorker(): SagaIterator {
    try {
        const result: {genres:Array<number>} = yield call(requestFunction, apiRoutes.genres, 'GET');
        yield put(setGenresList(result?.genres));
    } catch (error) {
        console.warn("getGenresWorker ===>", error);
    }
}

export function* getGenresWatcher(): SagaIterator {
    yield takeEvery(actionTypes.SET_GENRES, getGenresWorker);
}

export function* getFilmsFirstWorker({payload}: ReturnType<typeof setFirstFilms>): SagaIterator {
    try {
        yield put(setIsLoadingAC(true));
        const result = yield call(requestFunction, `movie?page=${payload}`, 'GET');
        yield put(setFilmsList(result));
        yield put(setIsLoadingAC(false));
    } catch (error) {
        console.warn("getFilmsFirstWorker ===>", error);
    }
}

export function* getFilmsFirstWatcher(): SagaIterator {
    yield takeEvery(actionTypes.SET_FIRST_FILMS, getFilmsFirstWorker);
}