export enum actionTypes {
    SET_FILMS = 'SET_FILMS',
    SET_GENRES = 'SET_GENRES',
    SET_FIRST_FILMS = 'SET_FIRST_FILMS'
}
