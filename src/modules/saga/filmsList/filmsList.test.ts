import { expectSaga } from 'redux-saga-test-plan';
import { call } from 'redux-saga/effects';
import { getFilmsFirstWorker, getFilmsWorker } from '.';
import { getGenresWorker } from '../filmsList';
import filmsReducer from '../../redux/filmsList/reducer';
import { setFilms, setFirstFilms, setGenres } from './action';
import { setGenresList } from '../../redux/common/action';
import { setFilmsList, setIsLoadingAC } from '../../redux/filmsList/action';
import { apiRoutes, requestFunction } from '../../../services/apiRoutes';

describe('filmsList saga test', () => {
    const state = {
        filmsList: []
    }

    it('getFilmsWorker test', async () => {
        const resolve = {
            movies: [{
                poster_path: 'poster_path',
                genre_ids: [1,2],
                title: 'title'
            }]
        }
        const payload = 1;
        const saga = expectSaga(getFilmsWorker, {
            type: '',
            payload
        })
        .provide([
            [call(requestFunction, `movie?page=${payload}`, 'GET'), resolve] 
            //@ts-ignore
        ]).withReducer(filmsReducer, state)
       
        const result = await saga
        .call(requestFunction, `movie?page=${payload}`, 'GET')
        //@ts-ignore
        .put(setFilmsList(resolve))
        .run()
        expect(result.effects).toEqual({})
    })

    it('setFilms action test', () => {
        //@ts-ignore
        const films = setFilms()
        expect(films).toEqual({type: 'SET_FILMS'})
    });

    it('getFilmsFirstWorker test', async () => {
        const resolve = {
            movies: [{
                poster_path: 'poster_path',
                genre_ids: [1,2],
                title: 'title'
            }]
        }
        const payload = 1;
        const saga = expectSaga(getFilmsFirstWorker, {
            type: '',
            payload
        })
        .provide([
            [call(requestFunction, `movie?page=${payload}`, 'GET'), resolve] 
            //@ts-ignore
        ]).withReducer(filmsReducer, state)
       
        const result = await saga
        .put(setIsLoadingAC(true))
        .call(requestFunction, `movie?page=${payload}`, 'GET')
        //@ts-ignore
        .put(setFilmsList(resolve))
        .put(setIsLoadingAC(false))
        .run()
        expect(result.effects).toEqual({})
    })

    it('setFirstFilms action test', () => {
        //@ts-ignore
        const firstFilms = setFirstFilms()
        expect(firstFilms).toEqual({type: 'SET_FIRST_FILMS'})
    });

    it('getGenresWorker test', async () => {
        const resolve = {
            genres: [{
                id: 1,
                name: '',
            }]
        }
        const saga = expectSaga(getGenresWorker)
        .provide([
            [call(requestFunction, apiRoutes.genres, 'GET'), resolve] 
            //@ts-ignore
        ]).withReducer(filmsReducer, state)
       
        const result = await saga
        .call(requestFunction, apiRoutes.genres, 'GET')
        .put(setGenresList(resolve.genres))
        .run()
        expect(result.effects).toEqual({})
    })
    
    it('setGenres action test', () => {
        const genres = setGenres()
        expect(genres).toEqual({type: 'SET_GENRES'})
    });
})