import { actionTypesFiltersSaga } from './actionTypes';
import { Action } from 'redux';
import { IFilterPayload } from 'components/FilterButtons';

export const getAllFilms = (payload: IFilterPayload): { type: actionTypesFiltersSaga.GET_ALL_FILMS, payload: IFilterPayload } => ({
    type: actionTypesFiltersSaga.GET_ALL_FILMS,
    payload
});

export const getLanguages = (): Action<actionTypesFiltersSaga.GET_LANGUAGES> => ({
    type: actionTypesFiltersSaga.GET_LANGUAGES,
});

export const getSearch = (payload:string) => ({
    type: actionTypesFiltersSaga.GET_SEARCH,
    payload
});
