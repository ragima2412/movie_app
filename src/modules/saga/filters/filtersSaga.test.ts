import { selectPopularFilter, selectBudget, selectRevenue, selectYear, selectLanguage } from '../../redux/rootReducer';
import { filterWorker, languageWorker, searchWorker } from ".";
import { expectSaga } from 'redux-saga-test-plan';
import { call, put, select } from 'redux-saga/effects';
import { apiRoutes, requestFunction } from '../../../services/apiRoutes';
import { setLanguagesList } from '../../redux/common/action';
import commonReducer from '../../redux/common/reducer';
import { setBudgetAC, setLanguageAC, setPopularAC, setRevenueAC, setSearchAC, setYearAC } from '../../redux/filters/action';
import { setAllFilms, setIsFiltered, setIsLoadingAC } from '../../redux/filmsList/action';
import {getAllFilms, getLanguages , getSearch} from './action';
import filtersReducer from '../../redux/filters/reducer';
import { selectLanguages } from '../../redux/rootReducer';

const filters = {
    value: ['Languages', ''],
    budget: ['Budget', ''],
    popular: ['Popular', ''],
    revenue: ['Revenue', ''],
    search: '',
    years: ['', '']
}
const common = {
    genres: [],
    languages: [],
}
describe('Filters Saga Tests', () => {

    test('languageWorker test', async () => {
        const saga = expectSaga(languageWorker)
            .provide([
                [call(requestFunction, `${apiRoutes.languages}`, 'GET'), { languages: [{ name: '', value: '' }] }],
                [put(setLanguagesList([{ name: '', value: '' }])), [{}]]
            ]).withReducer(commonReducer, common);

        const result = await saga
            .call(requestFunction, `${apiRoutes.languages}`, 'GET')
            .put(setLanguagesList([{ name: '', value: '' }]))
            .run()

        expect(result.effects).toEqual({})
    });

    test('Budget test', async () => {
        const payload = {
            budget: ['a', 'a'],
            value: 'a',
            popular: ['a', 'a'],
            minYear: 'a',
            maxYear: 'a',
            revenue: ['a', 'a'],
        }
        const saga = expectSaga(filterWorker, {
            //@ts-ignore
            type: '',
            payload
        })
            .provide([
                [select(selectLanguages), ['a', 'a']],
                [select(selectBudget), ['a', 'a']],
                [select(selectPopularFilter), ['a', 'a']],
                [select(selectRevenue), ['a', 'a']],
                [select(selectYear), ['a', 'a']],
                [call(requestFunction, `${apiRoutes.movie}?page=${1}&${apiRoutes.budget_min}=${['a'] && ['a']}&${apiRoutes.budget_max}=${['a']}&${apiRoutes.language}=${['a']}&${apiRoutes.popularity_min}=${['a'] && ['a']}&${apiRoutes.popularity_max}=${['a']}&${apiRoutes.release_date_first}=${['a']}&${apiRoutes.release_date_last}=${['a']}&${apiRoutes.revenue_min}=${['a'] && ['a']}&${apiRoutes.revenue_max}=${['a']}`, 'GET'), ''],
            ]).withReducer(filtersReducer, filters);

        const result = await saga
            .put(setIsLoadingAC(true))
            .put(setBudgetAC(payload.budget))
            .select(selectLanguage)
            .select(selectBudget)
            .select(selectPopularFilter)
            .select(selectRevenue)
            .select(selectYear)
            .call(requestFunction, `${apiRoutes.movie}?page=${1}&${apiRoutes.budget_min}=${['a'] && ['a']}&${apiRoutes.budget_max}=${['a']}&${apiRoutes.language}=${['a']}&${apiRoutes.popularity_min}=${['a'] && ['a']}&${apiRoutes.popularity_max}=${['a']}&${apiRoutes.release_date_first}=${['a']}&${apiRoutes.release_date_last}=${['a']}&${apiRoutes.revenue_min}=${['a'] && ['a']}&${apiRoutes.revenue_max}=${['a']}`, 'GET')
            .put(setAllFilms([]))
            .put(setIsLoadingAC(false))
            .run()

        expect(result.effects).toEqual({})
    });

    test('Language test', async () => {
        const payload = {
            budget: ['', ''],
            value: 'a',
            popular: ['', ''],
            minYear: '',
            maxYear: '',
            revenue: ['', ''],
        }
        const saga = expectSaga(filterWorker, {
            //@ts-ignore
            type: '',
            payload
        })
            .provide([
                [select(selectLanguage), ['a', 'a']],
                [select(selectBudget), ['', '']],
                [select(selectPopularFilter), ['', '']],
                [select(selectRevenue), ['', '']],
                [select(selectYear), ['', '']],
                [call(requestFunction, `${apiRoutes.movie}?page=${1}&${apiRoutes.budget_min}=${[''] && ['']}&${apiRoutes.budget_max}=${['']}&${apiRoutes.language}=${['a']}&${apiRoutes.popularity_min}=${[''] && ['']}&${apiRoutes.popularity_max}=${['']}&${apiRoutes.release_date_first}=${['']}&${apiRoutes.release_date_last}=${['']}&${apiRoutes.revenue_min}=${[''] && ['']}&${apiRoutes.revenue_max}=${['']}`, 'GET'), ''],
            ]).withReducer(filtersReducer, filters);

        const result = await saga
            .put(setIsLoadingAC(true))
            .put(setLanguageAC(payload.value))
            .select(selectLanguage)
            .select(selectBudget)
            .select(selectPopularFilter)
            .select(selectRevenue)
            .select(selectYear)
            .call(requestFunction, `${apiRoutes.movie}?page=${1}&${apiRoutes.budget_min}=${[''] && ['']}&${apiRoutes.budget_max}=${['']}&${apiRoutes.language}=${['a']}&${apiRoutes.popularity_min}=${[''] && ['']}&${apiRoutes.popularity_max}=${['']}&${apiRoutes.release_date_first}=${['']}&${apiRoutes.release_date_last}=${['']}&${apiRoutes.revenue_min}=${[''] && ['']}&${apiRoutes.revenue_max}=${['']}`, 'GET')
            .put(setAllFilms([]))
            .put(setIsLoadingAC(false))
            .run()

        expect(result.effects).toEqual({})
    });

    test('Popular test', async () => {
        const payload = {
            budget: ['', ''],
            value: '',
            popular: ['a', 'a'],
            minYear: '',
            maxYear: '',
            revenue: ['', ''],
        }
        const saga = expectSaga(filterWorker, {
            //@ts-ignore
            type: '',
            payload
        })
            .provide([
                [select(selectLanguage), ['', '']],
                [select(selectBudget), ['', '']],
                [select(selectPopularFilter), ['a', 'a']],
                [select(selectRevenue), ['', '']],
                [select(selectYear), ['', '']],
                [call(requestFunction, `${apiRoutes.movie}?page=${1}&${apiRoutes.budget_min}=${[''] && ['']}&${apiRoutes.budget_max}=${['']}&${apiRoutes.language}=${['']}&${apiRoutes.popularity_min}=${['a'] && ['a']}&${apiRoutes.popularity_max}=${['a']}&${apiRoutes.release_date_first}=${['']}&${apiRoutes.release_date_last}=${['']}&${apiRoutes.revenue_min}=${[''] && ['']}&${apiRoutes.revenue_max}=${['']}`, 'GET'), ''],
            ]).withReducer(filtersReducer, filters);

        const result = await saga
            .put(setIsLoadingAC(true))
            .put(setPopularAC(payload.popular))
            .select(selectLanguage)
            .select(selectBudget)
            .select(selectPopularFilter)
            .select(selectRevenue)
            .select(selectYear)
            .call(requestFunction, `${apiRoutes.movie}?page=${1}&${apiRoutes.budget_min}=${[''] && ['']}&${apiRoutes.budget_max}=${['']}&${apiRoutes.language}=${['']}&${apiRoutes.popularity_min}=${['a'] && ['a']}&${apiRoutes.popularity_max}=${['a']}&${apiRoutes.release_date_first}=${['']}&${apiRoutes.release_date_last}=${['']}&${apiRoutes.revenue_min}=${[''] && ['']}&${apiRoutes.revenue_max}=${['']}`, 'GET')
            .put(setAllFilms([]))
            .put(setIsLoadingAC(false))
            .run()

        expect(result.effects).toEqual({})
    });

    test('Revenue test', async () => {
        const payload = {
            budget: ['', ''],
            value: '',
            popular: ['', ''],
            minYear: '',
            maxYear: '',
            revenue: ['a', 'a'],
        }
        const saga = expectSaga(filterWorker, {
            //@ts-ignore
            type: '',
            payload,
        })
            .provide([
                [select(selectLanguage), ['', '']],
                [select(selectBudget), ['', '']],
                [select(selectPopularFilter), ['', '']],
                [select(selectRevenue), ['a', 'a']],
                [select(selectYear), ['', '']],
                [call(requestFunction, `${apiRoutes.movie}?page=${1}&${apiRoutes.budget_min}=${[''] && ['']}&${apiRoutes.budget_max}=${['']}&${apiRoutes.language}=${['']}&${apiRoutes.popularity_min}=${[''] && ['']}&${apiRoutes.popularity_max}=${['']}&${apiRoutes.release_date_first}=${['']}&${apiRoutes.release_date_last}=${['']}&${apiRoutes.revenue_min}=${['a'] && ['a']}&${apiRoutes.revenue_max}=${['a']}`, 'GET'), ''],
            ]).withReducer(filtersReducer, filters);

        const result = await saga
            .put(setRevenueAC(payload.revenue))
            .put(setIsLoadingAC(true))
            .select(selectLanguage)
            .select(selectBudget)
            .select(selectPopularFilter)
            .select(selectRevenue)
            .select(selectYear)
            .call(requestFunction, `${apiRoutes.movie}?page=${1}&${apiRoutes.budget_min}=${[''] && ['']}&${apiRoutes.budget_max}=${['']}&${apiRoutes.language}=${['']}&${apiRoutes.popularity_min}=${[''] && ['']}&${apiRoutes.popularity_max}=${['']}&${apiRoutes.release_date_first}=${['']}&${apiRoutes.release_date_last}=${['']}&${apiRoutes.revenue_min}=${['a'] && ['a']}&${apiRoutes.revenue_max}=${['a']}`, 'GET')
            .put(setAllFilms([]))
            .put(setIsLoadingAC(false))
            .run()

        expect(result.effects).toEqual({})
    });

    test('Years test', async () => {
        const payload = {
            budget: ['', ''],
            value: '',
            popular: ['', ''],
            minYear: 'a',
            maxYear: 'a',
            revenue: ['', ''],
        }
        const saga = expectSaga(filterWorker, {
            //@ts-ignore
            type: '',
            payload,
        })
            .provide([
                [select(selectLanguage), ['', '']],
                [select(selectBudget), ['', '']],
                [select(selectPopularFilter), ['', '']],
                [select(selectRevenue), ['', '']],
                [select(selectYear), ['a', 'a']],
                [call(requestFunction, `${apiRoutes.movie}?page=${1}&${apiRoutes.budget_min}=${[''] && ['']}&${apiRoutes.budget_max}=${['']}&${apiRoutes.language}=${['']}&${apiRoutes.popularity_min}=${[''] && ['']}&${apiRoutes.popularity_max}=${['']}&${apiRoutes.release_date_first}=${['a']}&${apiRoutes.release_date_last}=${['a']}&${apiRoutes.revenue_min}=${[''] && ['']}&${apiRoutes.revenue_max}=${['']}`, 'GET'), ''],
            ]).withReducer(filtersReducer, filters);

        const result = await saga
            .put(setIsLoadingAC(true))
            .put(setYearAC([payload.minYear, payload.maxYear]))
            .select(selectLanguage)
            .select(selectBudget)
            .select(selectPopularFilter)
            .select(selectRevenue)
            .select(selectYear)
            .call(requestFunction, `${apiRoutes.movie}?page=${1}&${apiRoutes.budget_min}=${[''] && ['']}&${apiRoutes.budget_max}=${['']}&${apiRoutes.language}=${['']}&${apiRoutes.popularity_min}=${[''] && ['']}&${apiRoutes.popularity_max}=${['']}&${apiRoutes.release_date_first}=${['a']}&${apiRoutes.release_date_last}=${['a']}&${apiRoutes.revenue_min}=${[''] && ['']}&${apiRoutes.revenue_max}=${['']}`, 'GET')
            .put(setAllFilms([]))
            .put(setIsLoadingAC(false))
            .run()

        expect(result.effects).toEqual({})
    });

    test('Search worker test with payload', async () => {
        const payload = 'a'
        const saga = expectSaga(searchWorker, {
            //@ts-ignore
            type: '',
            payload,
        })
            .provide([
                [call(requestFunction, `${apiRoutes.movie}?page=${1}&${apiRoutes.title}=${payload}`, 'GET'), ''],
            ]).withReducer(filtersReducer, filters);

        const result = await saga
            .put(setSearchAC('a'))
            .put(setIsLoadingAC(true))
            .call(requestFunction, `${apiRoutes.movie}?page=${1}&${apiRoutes.title}=${payload}`, 'GET')
            .put(setIsLoadingAC(false))
            .put(setAllFilms([]))
            .run()

        expect(result.effects).toEqual({})
    });

    test('Search worker test without payload', async () => {
        const payload = ''
        const saga = expectSaga(searchWorker, {
            //@ts-ignore
            type: '',
            payload,
        })
            .provide([
                [call(requestFunction, `${apiRoutes.movie}?page=${1}&${apiRoutes.title}=${payload}`, 'GET'), ''],
            ]).withReducer(filtersReducer, filters);

        const result = await saga
            .put(setSearchAC(''))
            .put(setIsLoadingAC(true))
            .put(setIsFiltered(false))
            .put(setIsLoadingAC(false))
            .put(setAllFilms([]))
            .run()

        expect(result.effects).toEqual({})
    });

    test("getAllFilms test", () => {
        const allFilms = getAllFilms({
            budget: ['', ''],
            value: '',
            popular: ['', ''],
            minYear: '',
            maxYear: '',
            revenue: ['', '']
        })
        expect(allFilms).toEqual({type: 'GET_ALL_FILMS', payload: {budget: ['', ''],
        value: '',
        popular: ['', ''],
        minYear: '',
        maxYear: '',
        revenue: ['', '']}})
    });

    test('getLanguages test', () => {
        const lang = getLanguages()
        expect(lang).toEqual({type: 'GET_LANGUAGES'})
    });

    test('getLanguages test', () => {
        const search = getSearch('')
        expect(search).toEqual({type: 'GET_SEARCH', payload: ''})
    });
});