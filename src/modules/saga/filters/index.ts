import { takeEvery, call, put, select } from 'redux-saga/effects';
import { setAllFilms, setIsFiltered, setIsLoadingAC } from '../../redux/filmsList/action';
import { actionTypesFiltersSaga } from './actionTypes';
import { setLanguagesList } from '../../redux/common/action';
import { apiRoutes, requestFunction } from '../../../services/apiRoutes';
import { SagaIterator } from '@redux-saga/types';
import { getAllFilms, getSearch } from './action';
import { setBudgetAC, setLanguageAC, setPopularAC, setRevenueAC, setSearchAC, setYearAC } from '../../redux/filters/action';
import { selectBudget, selectLanguage, selectPopularFilter, selectRevenue, selectYear } from '../../redux/rootReducer';

export function* languagesWatcher():SagaIterator {
    yield takeEvery(actionTypesFiltersSaga.GET_LANGUAGES, languageWorker);
}

export function* languageWorker(): SagaIterator {
    try {
        const {languages} = yield call(requestFunction, `${apiRoutes.languages}`, 'GET');
        yield put(setLanguagesList(languages));
    } catch (error) {
        console.warn(error);
    }
}

export function* filterWatcher(): SagaIterator {
    yield takeEvery(actionTypesFiltersSaga.GET_ALL_FILMS, filterWorker);
}

export function* filterWorker( {payload}: ReturnType<typeof getAllFilms> ): SagaIterator {
    const data: any = [];
    if(payload.budget[0]){
        yield put(setBudgetAC(payload.budget));
    }
    else if(payload.value){
        yield put (setLanguageAC(payload.value));
    }
    else if(payload.popular[0]){
        yield put (setPopularAC(payload.popular));
    }
    else if(payload.revenue[0]){
        yield put (setRevenueAC(payload.revenue));
    }
    else if(payload.minYear){
        yield put (setYearAC([payload.minYear,payload.maxYear]));
    }
    try {
        yield put(setIsLoadingAC(true));
        const lang = yield select(selectLanguage);
        const budg = yield select(selectBudget);
        const popul = yield select(selectPopularFilter);
        const reven = yield select(selectRevenue);
        const years = yield select(selectYear);
        for (let i = 1; i <= 5; i++) {
            const result: {movies:Array<object>} = yield call(requestFunction, `${apiRoutes.movie}?page=${i}&${apiRoutes.budget_min}=${budg[1] && budg[0]}&${apiRoutes.budget_max}=${budg[1]}&${apiRoutes.language}=${[lang[1]]}&${apiRoutes.popularity_min}=${popul[1] && popul[0]}&${apiRoutes.popularity_max}=${popul[1]}&${apiRoutes.release_date_first}=${years[0]}&${apiRoutes.release_date_last}=${years[1]}&${apiRoutes.revenue_min}=${reven[1] && reven[0]}&${apiRoutes.revenue_max}=${reven[1]}`,  'GET' );
            data.push(...result.movies);
        }
    } catch (error) {
        console.warn(error);
    }
    yield put(setAllFilms(data));
    yield put(setIsLoadingAC(false));
}

export function* searchWatcher() {
    yield takeEvery(actionTypesFiltersSaga.GET_SEARCH, searchWorker);
}

export function* searchWorker( {payload}: ReturnType<typeof getSearch> ) {
    yield put(setSearchAC(payload));
    const data: any = [];
    try {
        yield put(setIsLoadingAC(true));
        if (payload) {
            for (let i = 1; i <= 5; i++) {
                const result: {movies:Array<object>} = yield call(requestFunction, `${apiRoutes.movie}?page=${i}&${apiRoutes.title}=${payload}`, 'GET');
                data.push(...result.movies);
            }
        } else { yield put(setIsFiltered(false)); }
    } catch (error) {
        console.warn(error);
    }
    yield put(setIsLoadingAC(false));
    yield put(setAllFilms(data));
}