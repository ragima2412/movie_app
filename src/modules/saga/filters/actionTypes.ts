export enum actionTypesFiltersSaga  {
    GET_LANGUAGES = 'GET_LANGUAGES',
    GET_ALL_FILMS = 'GET_ALL_FILMS',
    GET_SEARCH = 'GET_SEARCH',
}