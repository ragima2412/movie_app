export const apiRoutes = {
    baseUrl: 'http://34.125.246.204/',
    posterUrl: 'http://image.tmdb.org/t/p/original/',
    movie: 'movie',
    genres: 'genres',
    page: 'page',
    popularity_min: 'popularity_min',
    popularity_max: 'popularity_max',
    release_date_first: 'release_date_first',
    release_date_last: 'release_date_last',
    sign_up: 'user/sing_up',
    budget_min: 'budget_min',
    budget_max: 'budget_max',
    languages: 'languages',
    language:'language',
    revenue_min: 'revenue_min',
    revenue_max: 'revenue_max',
    title: 'title',
    sign_in:'user/sign_in'
};

export const requestFunction = async (route: string, method: string, obj?: any,  headersKey?: {token: string}) => {
    const headers = {
        'Content-type': 'application/json'
    };
    const body = createBody(obj);
    const response = await fetch(apiRoutes.baseUrl + route, {
        method,
        headers,
        body,
    })
    .then(res => res.json())
    .catch(error => console.warn(`requestFunction route: ${route} ===>`, error));
    return response;
};

export const createBody = (object:any) => {
    return JSON.stringify(object);
};
