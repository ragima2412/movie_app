const formatDate = (date: Date) => {
    const d = new Date(date);
    const month = d.getMonth() + 1;
    const day = d.getDate();
    const year = d.getFullYear();
    return [year, month, day].join('-');
};

const minusYears = (date: Date, yearAmount: number) => {
    const year  = new Date(date).getFullYear();
    const month = new Date(date).getMonth();
    const day  = new Date(date).getDate();
    return date  = new Date(year - yearAmount, month, day);
};

const randomNums = (count: number, totalCount: number) => {
    const arr = [];
    while(arr.length < count){
        const num = Math.floor(Math.random() * totalCount) + 1;
        if(arr.indexOf(num) === -1) arr.push(num);
    }
    return arr;
};

export const Utils = {
    formatDate,
    minusYears,
    randomNums
};