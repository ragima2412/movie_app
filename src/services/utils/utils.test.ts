import { Utils } from '.';

describe('utils tests', () => {
    it('formatDate test', () => {
        const result = Utils.formatDate(new Date(2020, 11, 1));
        expect(result).toBe("2020-12-1");
    })

    it('minusYears test', () => {
        const result = Utils.minusYears(new Date(2020, 11, 1), 2);
        expect(result).toStrictEqual(new Date(2018, 11, 1));
    })

    it('randomNums test', () => {
        const result = Utils.randomNums(4, 100);
        expect(result.length).toBe(4);
        expect(result[0]).toBeGreaterThanOrEqual(1);
        expect(result[result.length-1]).toBeLessThanOrEqual(100);
    })
})