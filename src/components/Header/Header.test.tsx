import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { mocks } from "../../../mocks";
import Header from "./header";
//@ts-ignore
window.matchMedia = window.matchMedia || function() {
    return {
        matches: false,
        addListener: function() {},
        removeListener: function() {}
    };
};
describe('Headers Component Test', () => {
    it('snapshot test', () => {
        const component = render(
            <BrowserRouter>
            <Provider store={mocks.storeHeader}>
                <Header />
            </Provider>
            </BrowserRouter>
        );
        expect(component).toMatchSnapshot();
    });
})
// import Header from "./header";

// window.matchMedia = window.matchMedia || function() {
//     return {
//         matches: false,
//         addListener: function() {},
//         removeListener: function() {}
//     };
// };
// describe('Headers Component Test', () => {
//     it('snapshot test', () => {
//         const component = render(
//             <BrowserRouter>
//             <Provider store={mocks.storeHeader}>
//                 <Header />
//             </Provider>
//             </BrowserRouter>
//         );
//         expect(component).toMatchSnapshot();
//     });
// })

export default {};
