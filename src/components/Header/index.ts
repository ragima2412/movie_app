import { ApplicationState } from 'modules/redux';
import { setIsFiltered } from 'modules/redux/filmsList/action';
import { setActiveHomeLink, setActiveMoviesLink } from 'modules/redux/home/action';
import { setActiveButtonAC, setEmptyMessageSignInInputs, setInitialDataAC, setLogOut, setModalSignIn } from 'modules/redux/signIn/action';
import { setEmptyMessageSignUp, setModalSignUp } from 'modules/redux/signUp/action';
import { Dispatch, Action } from 'redux';
import { connect } from 'react-redux';
import { actionTypes } from 'modules/redux/signIn/actionTypes';
import { actionTypesSignUp } from 'modules/redux/signUp/actionTypes';
import { actionTypesHome } from 'modules/redux/home/actionTypes';
import { types } from 'modules/redux/filmsList/actionTypes';
import Header from './Header';
import { getUserProfile, selectHomeLink, selectLogOut, selectMoviesLink } from '../../modules/redux/rootReducer';

export type TPropsFromState = {
    data:  {
        login: string,
        password: string,
        token: string,
        message: {
            login:string,
            password:string,
            message:string,
        },
        userId: string,
        modalWindow: boolean,
        logOut: boolean,
        activeButton: boolean,
        isLoadingMessage: boolean
    },
    info: boolean,
    home: boolean,
    movies: boolean,
}

export type TPropsFromDispatch = {
    setActiveButton: (parameters: boolean) => Action<actionTypes.SET_ACTIVE_BUTTON>,
    setModalSignIn: (parameters: boolean) => Action<actionTypes.SET_MODAL_SIGN_IN>,
    setEmptyMessageSignUp: (payload : {first_name: '', last_name: '', login: '', password: ''}) => Action<actionTypesSignUp.SET_EMPTY_MESSAGE_SIGN_UP>,
    setEmptyMessageSignInInputs: (payload : {login: '', password: ''}) => Action<actionTypes.SET_EMPTY_MESSAGE_SIGN_IN_INPUTS>,
    setModalSignUp: (parameters: boolean) => Action<actionTypesSignUp.SET_MODAL_SIGN_UP>,
    setLogOut: (parameters: boolean) => Action<actionTypes.SET_LOG_OUT>,
    setInitialData: () => Action<actionTypes.SET_INITIAL_DATA>,
    setActiveHomeLink: (parameters: boolean) => Action<actionTypesHome.SET_ACTIVE_HOME_LINK>,
    setActiveMoviesLink: (parameters: boolean) => Action<actionTypesHome.SET_ACTIVE_MOVIES_LINK>
    setIsFiltered:  (parameters: boolean) => Action<types.SET_IS_FILTERED>
};

const mapStateToProps = (state: ApplicationState): TPropsFromState => {
    return {
        data: getUserProfile(state),
        info: selectLogOut(state),
        home: selectHomeLink(state),
        movies: selectMoviesLink(state),
    };
};

const mapDispatchToProps = (dispatch: Dispatch): TPropsFromDispatch => {
    return {    
        setActiveButton: (parameters: boolean) => {
        return dispatch(setActiveButtonAC(parameters));
        },
        setModalSignIn: (parameters: boolean) => {
            return dispatch(setModalSignIn(parameters));
        },
        setEmptyMessageSignUp: (payload : {first_name: '', last_name: '', login: '', password: ''}) => {
            return dispatch(setEmptyMessageSignUp(payload));
        },
        setEmptyMessageSignInInputs: (payload : {login: '', password: ''}) => {
            return dispatch(setEmptyMessageSignInInputs(payload));
        },
        setModalSignUp: (parameters: boolean) => {
            return dispatch(setModalSignUp(parameters));
        },
        setLogOut: (parameters: boolean) => {
            return dispatch(setLogOut(parameters));
        },
        setInitialData: () => {
            return  dispatch(setInitialDataAC());
        },
        setActiveHomeLink: (parameters: boolean) => {
            return dispatch(setActiveHomeLink(parameters));
        },
        setActiveMoviesLink: (parameters: boolean) => {
            return dispatch(setActiveMoviesLink(parameters));
        },
        setIsFiltered: (parameters: boolean) => {
            return dispatch(setIsFiltered(parameters));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
