import './Header.less';
import { useState } from 'react';
import * as React from 'react';
import { NavLink } from 'react-router-dom';
import { Layout, Button } from 'antd';
import ModalWindow from '../../screens/Modal';
import user from '../../assets/images/user.svg';
import PopUp from '../../screens/PopUp/index';
import { localization } from '../../modules/localization';
import { TPropsFromState, TPropsFromDispatch } from '.';

type TProps = TPropsFromState & TPropsFromDispatch;

const Header: React.FC<TProps> = ({
    setActiveButton,
    setModalSignIn,
    setEmptyMessageSignUp,
    setEmptyMessageSignInInputs,
    setModalSignUp,
    setLogOut,
    setInitialData,
    setActiveHomeLink,
    setActiveMoviesLink,
    setIsFiltered,
    data,
    info,
    home,
    movies
}) => {
    const [menu, setMenu] = useState(true);
    const [background, setBackground] = useState(false);
    const {Header} = Layout;

    const setMenuHandleClick = () => {
        setMenu((prev => !prev));
    };

    const setSignInMenu = () => {
        setActiveButton(true);
        setModalSignIn(true);
        setEmptyMessageSignUp({first_name: '', last_name: '', login: '', password: ''});
        setEmptyMessageSignInInputs({login: '', password: ''});
    };

    const setSignUpMenu = () => {
        setActiveButton(false);
        setModalSignIn(true);
        setModalSignUp(true);
        setEmptyMessageSignUp({first_name: '', last_name: '', login: '', password: ''});
        setEmptyMessageSignInInputs({login: '', password: ''});
    };

    const handleClick = () => {
        setBackground(true);
        setLogOut(true);
    };

    const showBackground = () => {
        setBackground(!background);
    };

    const logOut = () => {
        setInitialData();
        setActiveHomeLink(true);
        setActiveMoviesLink(false);
    };

    const handleIsFilteredChange = () => {
        setIsFiltered(false);
    };

    const closeMobileMenu = () => {
        setMenu(true);
    };

    return (
        <Layout className="header-layout">
            {background && info
                ? <>
                    <div className="absolute-bg" onClick={showBackground}/>
                    <PopUp/></>
                : <></>}
            <Header className="header">
                <NavLink to="/" className="logo">{localization.MOVIES}</NavLink>
                <div className="header-menu-mob" onClick={setMenuHandleClick}>
                    <i className={menu ? 'fas fa-bars' : 'fas fa-times'}/>
                </div>
                <div className={menu ? 'menu-pc' : 'menu-mob'}>
                    <div className="menu-nav-wrapper">
                        <div className="navlink-item" onClick={handleIsFilteredChange}>
                            <NavLink to="/" className={home ? 'active-navlink' : 'deactive-navlink'}
                                     onClick={closeMobileMenu}>{localization.HOME}</NavLink>
                            <span className={home ? "line-home" : ""}/>
                        </div>
                        <div className="navlink-item">
                            <NavLink to="/movies" className={movies ? 'active-navlink' : 'deactive-navlink'}
                                     onClick={closeMobileMenu}>{localization.MOVIES}</NavLink>
                            <span className={movies ? 'line-movies' : ''}/>
                        </div>
                    </div>
                    {data?.token
                        ? <div className="exit-wrapper" onClick={handleClick}>
                            <img src={user} alt="user" className="user-image"/>
                        </div>
                        : <div className="buttons">
                            <Button className="sign-in ant-btn-primary"
                                    onClick={setSignInMenu}>{localization.SIGN_IN}</Button>
                            <Button className="sign-in ant-btn-primary"
                                    onClick={setSignUpMenu}>{localization.SIGN_UP}</Button>
                        </div>}
                    {data.token
                        ? <ul className="exit-wrapper-mob">
                            <li className="user-name">{data.login}</li>
                            <li onClick={logOut}>
                                <NavLink to="/" className="log-out-link">{localization.LOG_OUT}</NavLink>
                            </li>
                        </ul>
                        : <></>
                    }
                </div>
            </Header>
            <ModalWindow/>
        </Layout>
    );
};

export default Header;
