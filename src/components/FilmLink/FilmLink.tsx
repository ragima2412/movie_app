import { Link } from 'react-router-dom';
import * as React from 'react';
import { apiRoutes } from '../../services/apiRoutes';
import Genres from '../../components/Genres';
import { TPassedProps, TPropsFromDispatch } from '.';
import { IFilm } from 'modules/redux/filmDetail/types';

type TProps = TPropsFromDispatch & TPassedProps;

const FilmLink: React.FC<TProps>  = ({ setFilmDetail, item }) => {
    
    const handleItemClick = (item: IFilm) => () => {
        setFilmDetail(item);
       };
    return (
        <div className="film-link">
            <Link to="/film"  onContextMenu={handleItemClick(item)} onClick={handleItemClick(item)}>
                <div className="poster" style={{backgroundImage: `url(${apiRoutes.posterUrl + item.poster_path})`}}>
                    <div className="film-card">
                        <div className="poster-info">
                            {item?.genre_ids?.map((genre_id: number) => (
                                <Genres key={genre_id} genreId={genre_id}/>
                            ))}
                            <div className="title">{item?.title}</div>
                        </div>
                    </div>
                </div>
            </Link>
        </div>
    );
};
export default FilmLink;
