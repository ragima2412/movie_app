import { render } from "@testing-library/react";
import Footer from "./Footer";

describe('Footer Component Test', () => {
    it('snapshot test', () => {
        const component = render(
                <Footer />
        );
        expect(component).toMatchSnapshot();
    });
})