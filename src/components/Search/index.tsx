import './Search.less';
import * as React from 'react';
import { Button, Input } from 'antd';
import { setIsFiltered } from 'modules/redux/filmsList/action';
import { setClear } from 'modules/redux/filters/action';
import { getSearch } from 'modules/saga/filters/action';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { selectSearch } from 'modules/redux/rootReducer';

const Search = () => {
    const [searchItem, setSearchItem] = useState('');
    const dispatch = useDispatch();
    const searchedItem = useSelector(selectSearch);

    const handleSearch = () => {
        dispatch(setClear());
        dispatch(setIsFiltered(true));
        dispatch(getSearch(searchItem));
    };
    
    const handleChangeInput = (value: string) => {
        setSearchItem(value);
    };

    return (
        <div className="search-components">
            <div className='search-block'>
                <Input onChange={(value)=>handleChangeInput(value.target.value)} className="search" placeholder={searchedItem ? searchedItem : 'Search'}></Input>
            </div>
            <div className='search-btn-block'>
                <Button onClick={handleSearch} className="search-button">Search</Button>
            </div>
        </div>
    );
};
export default Search;