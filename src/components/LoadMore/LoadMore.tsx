import './LoadMore.less';
import { MouseEventHandler } from 'react';
import * as React from 'react';

type TProps = {
    currentPage: number,
    totalPages: number,
    getMore: MouseEventHandler
}

const LoadMoreButton: React.FC<TProps> = ({currentPage, totalPages, getMore}) => {
    
    return (
        <div className = {`list-btn ${(currentPage === totalPages + 1) ? 'hide-load-more' : 'show-load-more'}`}>
            <button className="show-more" onClick={getMore}>Show more</button>
        </div>
    );
};

export default LoadMoreButton;