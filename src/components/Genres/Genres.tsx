import * as React from 'react';

type TProps = {
    genreId: number,
    genres: Array<{id: number, name: string}>,
}

const Genres: React.FC<TProps> = ({genres, genreId}) => {
    return (
        <div className="genre">
            {genres?.find((genre: {id: number, name: string}) => (
                genre?.id === genreId
            ))?.name}
        </div>
    );
};

export default Genres;
