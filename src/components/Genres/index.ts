import { ApplicationState } from 'modules/redux';
import { selectGenres } from 'modules/redux/rootReducer';
import { connect } from 'react-redux';
import Genres from './Genres';

export type TPropsFromState = {
  genres: Array<{id: number, name: string}>
}

const mapStateToProps = (state: ApplicationState): TPropsFromState => {
  return {
      genres: selectGenres(state),
    };
  };
  
export default connect(mapStateToProps)(Genres);
