import Filters from './Filters';
import { connect } from 'react-redux';
import { getAllFilms, getLanguages } from 'modules/saga/filters/action';
import { setClear, setClearSearch } from 'modules/redux/filters/action';
import { setIsFiltered } from 'modules/redux/filmsList/action';
import { ApplicationState } from 'modules/redux';
import { Dispatch, Action } from 'redux';
import { actionTypesFiltersSaga } from 'modules/saga/filters/actionTypes';
import { filterTypesRedux } from 'modules/redux/filters/actionTypes';
import { types } from 'modules/redux/filmsList/actionTypes';
import { selectLanguages } from 'modules/redux/rootReducer';
import { selectBudget, selectLanguage, selectPopularFilter, selectRevenue, selectYear } from 'modules/redux/rootReducer';

export type IFilterPayload = {
        budget: Array<string>,
        value: string,
        popular:Array<string>,
        minYear: string,
        maxYear: string,
        revenue: Array<string>,
}
export type TPropsFromDispatch = {
    getLanguages: () => Action<actionTypesFiltersSaga.GET_LANGUAGES>,
    setClear: () => Action<filterTypesRedux.SET_CLEAR>,
    getAllFilms: (info : IFilterPayload ) => Action<actionTypesFiltersSaga.GET_ALL_FILMS>,
    setIsFiltered: (isFilter: boolean) => Action<types.SET_IS_FILTERED>
    setClearSearch: () => Action<filterTypesRedux.SET_CLEAR_SEARCH>
}
export type TPropsFromState = {
    languages: Array<{ value: string, name: string }>
    language: Array<string>,
    budget: Array<string> ,
    popular: Array<string> ,
    revenue: Array<string> ,
    rangeYear: Array<string>
}

const mapStateToProps = (state: ApplicationState): TPropsFromState => {
    return {
        languages: selectLanguages(state),
        language: selectLanguage(state),
        budget: selectBudget(state),
        popular: selectPopularFilter(state),
        revenue: selectRevenue(state),
        rangeYear: selectYear(state)
    };
};

const mapDispatchToProps = (dispatch: Dispatch<Action>): TPropsFromDispatch => {
    return {
        getLanguages: () => dispatch(getLanguages()),
        setClear: () => dispatch(setClear()),
        getAllFilms: (info: IFilterPayload) => dispatch(getAllFilms(info)),
        setIsFiltered: (isFilter: boolean) => dispatch(setIsFiltered(isFilter)),
        setClearSearch: () => dispatch(setClearSearch())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Filters);
