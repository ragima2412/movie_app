import { act, render } from '@testing-library/react';
import { mount } from 'enzyme';
import moment from 'moment';
import { Provider } from 'react-redux';
import { mocks } from '../../../mocks';
import Filters from '../FilterButtons/Filters';

jest.mock('src/components/Search', () => () => <div id="Search" />);

describe('Filters Component Test', () => {
    let props:any;
  beforeEach(() => {
    props = {
        languages: [{ name:"Azeri", value: 'string'}],
        language: ["Azeri","Aazeri"],
        budget: ["Azeri","Aazeri"],
        popular: ["Azeri","Aazeri"],
        revenue: ["Azeri","Aazeri"],
        rangeYear: ["Azeri","Aazeri"],
        getLanguages: jest.fn(),
        setClear: jest.fn(),
        getAllFilms:  jest.fn(),
        setIsFiltered:  jest.fn(),
        updateReport: jest.fn()
    }
})
    it('snapshot test', () => {
        const component = render(
            <Provider store={mocks.storeFilters}>
                <Filters {...props}/>
            </Provider>
        );
        expect(component).toMatchSnapshot();
    })

    it('Select test revenue', () => {
        const component = mount(
            <Provider store={mocks.storeFilters}>
                <Filters {...props}/>
            </Provider>
        )
        act(() => {
            component.find('#revenue').first().getElement().props.onChange("test,test")
        });
        component.update()
        expect(props.setIsFiltered).toHaveBeenCalled()
    });
    
    it('Select test language', () => {
        const component = mount(
            <Provider store={mocks.storeFilters}>
                <Filters {...props}/>
            </Provider>
        )
        act(() => {
            component.find('#language').first().getElement().props.onChange("test,test")
        });
        component.update()
        expect(props.setIsFiltered).toHaveBeenCalled()
    });

    it('Select test popular', () => {
        const component = mount(
            <Provider store={mocks.storeFilters}>
                <Filters {...props}/>
            </Provider>
        )
        act(() => {
            component.find('#popular').first().getElement().props.onChange("test,test")
        });
        component.update()
        expect(props.setIsFiltered).toHaveBeenCalled()
    });

    it('Select test budget', () => {
        const component = mount(
            <Provider store={mocks.storeFilters}>
                <Filters {...props}/>
            </Provider>
        )
        act(() => {
            component.find('.selectors').first().getElement().props.onChange("test,test")
        });
        component.update()
        expect(props.setIsFiltered).toHaveBeenCalled()
    });
    it('Select test date', () => {
        const date = [moment('2020-12-30T07:36:41.928Z'),moment('2020-12-30T07:36:41.928Z')]
        const component = mount(
            <Provider store={mocks.storeFilters}>
                <Filters {...props}/>
            </Provider>
        )
        act(() => {
            component.find('.date').first().getElement().props.onChange(date);
        });

        component.update();
        expect(props.setIsFiltered).toHaveBeenCalled();
    });

    it('Select test clear button', () => {
        const component = mount(
            <Provider store={mocks.storeFilters}>
                <Filters {...props}/>
            </Provider>
        )
        act(() => {
            component.find('#buttonClear').first().getElement().props.onClick();
        });

        component.update();
        expect(props.setClear).toHaveBeenCalled();
    });
});
