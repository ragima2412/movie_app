import './Filters.less';
import * as React from 'react';
import { FC, useEffect, useState } from 'react';
import { DatePicker, Button, Select } from 'antd';
import Search from 'components/Search';
import { TPropsFromDispatch, TPropsFromState } from '.';

type TProps = TPropsFromDispatch & TPropsFromState;

const Filters: FC<TProps> = ({
    languages,
    language,
    budget,
    popular,
    revenue,
    rangeYear,
    getLanguages,
    setClear,
    getAllFilms,
    setIsFiltered,
    setClearSearch
}) => {
    const {RangePicker} = DatePicker;
    const {Option} = Select;
    const [info, setInfo] = useState({value: '', budget: [''], popular: [''], revenue: [''], minYear: '', maxYear: ''});
    const [isFilterChange, setIsFilterChange] = useState(false);

    useEffect(() => {
        getLanguages();
        setInfo({value: '', budget: [''], popular: [''], revenue: [''], minYear: '', maxYear: ''});
    }, []);

    useEffect(() => {
        if (isFilterChange) {
            getAllFilms(info);
            setClearSearch();
        }
    }, [info]);

    const onFilterLanguageChange = (value: string) => {
        setInfo({...info, value});
        setIsFilterChange(true);
        setIsFiltered(true);
    };
    const onFilterYearsChangeMin = (dateString: any) => {
        setInfo({...info, minYear: dateString[0].format().slice(0, 10), maxYear: dateString[1].format().slice(0, 10)});
        setIsFilterChange(true);
        setIsFiltered(true);
    };
    const onFilterBudgetChange = (e: string) => {
        setInfo({...info, budget: e.split(',')});
        setIsFilterChange(true);
        setIsFiltered(true);
    };
    const onFilterPopularChange = (e: string) => {
        setInfo({...info, popular: e.split(',')});
        setIsFilterChange(true);
        setIsFiltered(true);
    };
    const onFilterRevenueChange = (e: string) => {
        setInfo({...info, revenue: e.split(',')});
        setIsFilterChange(true);
        setIsFiltered(true);
    };
    const resetFilters = () => {
        setInfo({value: '', budget: [''], popular: [''], revenue: [''], minYear: '', maxYear: ''});
        setIsFilterChange(false);
        setClear();
        setIsFiltered(false);
    };
    return (
        <div className="search-filter">
            <Search/>
            <div className="filters-block">
                <div className="filters">
                    <Select dropdownStyle={{backgroundColor: '#1C232D'}} test-data="budget" className="selectors"
                            value={budget[1] ? `${budget[0]}-${budget[1]}` : budget[0]} onChange={onFilterBudgetChange}>
                        <Option test-data="budget1value" className="option" value="0,100000">0 $ -100 000 $</Option>
                        <Option test-data="budget2value" className="option" value="100001,500000">100 001 $ - 500 000
                            $</Option>
                        <Option test-data="budget3value" className="option" value="500001,1000000">500 001 $ - 1 000 000
                            $</Option>
                        <Option test-data="budget4value" className="option" value="1000001,100000000">1 000 001 $ - 100
                            000 000 $</Option>
                        <Option test-data="budget5value" className="option" value="100000001,500000000">100 000 001 $ -
                            500 000 000 $</Option>
                    </Select>
                    <Select dropdownStyle={{backgroundColor: '#1C232D'}} id="language" test-data="language"
                            className="selectors" value={language[1] ? language[1] : language[0]}
                            onChange={onFilterLanguageChange}>
                        {
                            languages.map((item: { value: string, name: string }) =>
                                <Option test-data="languageValue" key={item.value} className="option"
                                        value={item.value}>{item?.name}</Option>
                            )
                        }
                    </Select>
                    <Select dropdownStyle={{backgroundColor: '#1C232D'}} id="popular" test-data="popular"
                            className="selectors" value={popular[1] ? `${popular[0]}-${popular[1]}` : popular[0]}
                            onChange={onFilterPopularChange}>
                        <Option test-data="popular1Value" className="option" value="0,100">0 - 100</Option>
                        <Option test-data="popular2Value" className="option" value="101,200">101 - 200</Option>
                        <Option test-data="popular3Value" className="option" value="201,300">201 - 300</Option>
                        <Option test-data="popular4Value" className="option" value="301,400">301 - 400</Option>
                        <Option test-data="popular5Value" className="option" value="401,500">401 - 500</Option>
                        <Option test-data="popular6Value" className="option" value="501,900">501 - 900</Option>
                    </Select>
                    <Select dropdownStyle={{backgroundColor: '#1C232D'}} id="revenue" test-data="revenue"
                            className="selectors" value={revenue[1] ? `${revenue[0]}-${revenue[1]}` : revenue[0]}
                            onChange={onFilterRevenueChange}>
                        <Option test-data="revenue1Value" className="option" value="0,100000">0 $ - 100 000 $</Option>
                        <Option test-data="revenue2Value" className="option" value="100001,500000">100 001 $ - 500 000
                            $</Option>
                        <Option test-data="revenue3Value" className="option" value="500001,1000000">500 001 $ - 1 000
                            000 $</Option>
                        <Option test-data="revenue4Value" className="option" value="1000001,100000000">1 000 001 $ - 100
                            000 000 $</Option>
                        <Option test-data="revenue5Value" className="option" value="100000001,1000000000">100 000 001 $
                            - 1 000 000 000 $</Option>
                        <Option test-data="revenue6Value" className="option" value="1000000001,10000000000">1 000 000
                            001 $ - 10 000 000 000 $</Option>
                    </Select>
                    <RangePicker
                        placeholder={rangeYear[0] ? [`${rangeYear[0]}`, `${rangeYear[1]}`] : ['Start date', 'End date']}
                        test-data="calendar" className="date selectors" style={{color: 'white'}}
                        onChange={onFilterYearsChangeMin}/>
                </div>
                <div className="clear-btn-block">
                    <Button className="clear-button" id="buttonClear" onClick={resetFilters}>
                        Clear Filters
                    </Button>
                </div>
            </div>
        </div>
    );
};
export default Filters;