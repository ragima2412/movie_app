import './RandomSlider.less';
import { Carousel } from 'antd';
import * as React from 'react';
import { ISlider } from './types';
import { Link } from 'react-router-dom';
import { apiRoutes } from '../../services/apiRoutes';
import { IFilm } from 'modules/redux/filmDetail/types';
import { TPropsFromDispatch } from '.';

type TProps = TPropsFromDispatch & ISlider;

const RandomSlider: React.FC<TProps>  = ({ list, setFilmDetail }) => {

    const setFilmId = (item: IFilm) => {
        setFilmDetail(item);
    };
    
    const handleItemClick = (item: IFilm) => () => {
         setFilmId(item);
    };

    return (
        <div className="random-carousel">
            <Carousel autoplay>
                {list?.map((item: IFilm, index) => (
                    <Link to='/film' onContextMenu={handleItemClick(item)} key={index} onClick={handleItemClick(item)}>
                        <div key={index} className="poster-container">
                            <img src={`${apiRoutes.posterUrl + item.backdrop_path}`} className="poster" alt="film poster" />
                        </div>
                    </Link>
                ))}
            </Carousel>
        </div>
    );
};

export default RandomSlider;

