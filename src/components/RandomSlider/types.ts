import { IFilm } from "modules/redux/filmDetail/types";

export interface ISlider {
	list: Array<IFilm>
}

export interface IPayload {
    id: number;
    backdrop_path: string;
    genre_ids?:Array<number>;
    poster_path?:string;
    title?: string;
}