import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { mocks } from "../../../mocks";
import RandomSlider from "./RandomSlider";
import { ISlider } from "./types";
import * as React from 'react';



// jest.mock('src/components/Search', () => () => <div id="Search" />);

const props: ISlider = {
    list: [{
        backdrop_path: 'backdrop_path',
        genre_ids: [1, 2],
        poster_path: 'poster_path',
        title: 'title',
    }]
};
describe('RandomSlider Component Test', () => {
    it('snapshot test', () => {
        const component = render(
            <BrowserRouter>
            {/* <Provider store={mocks.storeFilters}> */}
                <RandomSlider {...props} />
            {/* </Provider> */}
            </BrowserRouter>
        );
        expect(component).toMatchSnapshot();
    });
});