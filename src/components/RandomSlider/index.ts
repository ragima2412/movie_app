import { connect } from 'react-redux';
import { setAllFilmsAC, setFilmDetailAC } from '../../modules/saga/filmDetail/action';
import { Dispatch, Action } from 'redux';
import { actionTypes } from '../../modules/saga/filmDetail/actionTypes';
import { IFilm } from '../../modules/redux/filmDetail/types';
import RandomSlider from './RandomSlider';
 
export type TPropsFromDispatch = {
    setAllFilms: () => Action<actionTypes.SET_ALL_FILMS>,
    setFilmDetail: (item: IFilm ) => Action<actionTypes.SET_FILM_DETAIL>
}

const mapDispatchToProps = (dispatch: Dispatch<Action>): TPropsFromDispatch => {
    return {
        setAllFilms: () => {
            return dispatch(setAllFilmsAC());
        },
        setFilmDetail: (item: IFilm) => {
            return dispatch(setFilmDetailAC(item));
        }
    };
  };
  
export default connect(null, mapDispatchToProps)(RandomSlider);