import './Comments.less';
import * as React from 'react';
import { UserOutlined } from '@ant-design/icons';
import { Typography } from 'antd';

const Comments = () => {
    const { Paragraph } = Typography;
    return(
        <div className="comments-container">
            <UserOutlined />
            <div className="comment-text-container">
            <Paragraph>Guest  10.12.2021</Paragraph>
            <Paragraph className="comment-text">Lorem Ipsum is simply dummy text of the printing and 
                typesetting industry. Lorem Ipsum has been the industry`&apos;`s standard dummy text ever since the 1500s</Paragraph>
            </div>
        </div>
    );
};

export default Comments;