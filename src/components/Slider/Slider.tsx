import './Slider.less';
import * as React from 'react';
import { ISlider } from './types';
import { Carousel } from 'antd';
import { LeftCircleOutlined, RightCircleOutlined } from '@ant-design/icons';
import FilmLink from '../../components/FilmLink';
import { IFilm } from '../../modules/redux/filmDetail/types';

type TProps = ISlider;

const Slider: React.FC<TProps> = ({list, numberOfCards}) => {
    return (
        <div className="carousel">
            <Carousel
                arrows={true}
                prevArrow={<LeftCircleOutlined/>}
                nextArrow={<RightCircleOutlined/>}
                slidesToShow={numberOfCards}
                infinite={true}
                dots={false}
            >
                {list?.map((item: IFilm) => <FilmLink item={item} key={item.id}/>)}
            </Carousel>
        </div>
    );
};

export default Slider;