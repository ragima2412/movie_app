
export interface IFilm {
	poster_path: string;
	genre_ids: Array<number>;
	title: string;
}

export interface ISlider {
	list: Array<{
		id: number,
		poster_path: string,
		genre_ids: Array<number>,
		title: string,
		release_date:string,
		popularity:number,
		tagline: string,
		overview: string,
		adult?: boolean,
		runtime: number,
		budget: number,
		revenue: number,
		homepage: string,
		backdrop_path: string,
	}>,
	numberOfCards: number,
}