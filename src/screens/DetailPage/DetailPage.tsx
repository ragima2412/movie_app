import './DetailPage.less';
import * as React from 'react';
import 'react-circular-progressbar/dist/styles.css';
import { Typography, Button, Rate } from 'antd';
import { PlayCircleOutlined } from '@ant-design/icons';
import { TPropsFromState , TPropsFromDispatch } from '.';
import { FC } from 'react';
import { localization } from '../../modules/localization';
import Comments from '../../components/comments/Comments';

type TProps = TPropsFromState  & TPropsFromDispatch;

const DetailPage:FC<TProps> = ({filmDetailPage, genres, setActiveButton, setModalSignIn, setModalSignUp}) => {
    const { Title, Paragraph } = Typography;    
    const releaseDate = filmDetailPage?.release_date;
    const releaseYear = releaseDate?.split("-")[0];
    const setSignInMenu = () => {
        setActiveButton(true);
        setModalSignIn(true);
    };

    const setSignUpMenu = () => {
        setActiveButton(false);
        setModalSignIn(true);
        setModalSignUp(true);
    };

    return (
        <div className="detail-page">  
            <div className="detail-page-container" style={{ color: "white" }}>
                <div className="film-info-container">
                    <div className="film-image">
                        <img alt="film-image" src={`https://image.tmdb.org/t/p/original/${filmDetailPage.poster_path}`} />                       
                    </div>
                    <div className="film-info">
                        <div className="rate-and-title">
                            <Title  className="title" style={{ color: "white" }} level={2}>{filmDetailPage.title} </Title>
                            <Rate allowHalf defaultValue={2.5} />
                        </div>
                        <Paragraph className="film-overview" style={{ color: "white" }}>{filmDetailPage.overview} </Paragraph>
                        <div className="film-details">
                            <div className="left-side-details">
                                <Paragraph>{localization.RELEASE_DATE}:{" " + releaseYear}</Paragraph>
                                <Paragraph>{localization.GENRE}: {filmDetailPage?.genre_ids.map((genre_id:number, index:number) => {
                                    return <>
                                        <Paragraph className="film-genres" key={index}>  
                                            {genres.filter(genre => {
                                                return genre.id === genre_id;
                                            })[0]?.name + ""}
                                        </Paragraph>
                                    </>;
                                })}</Paragraph>
                                <Paragraph>{localization.RUNTIME}:
                                    {" " + filmDetailPage.runtime} min </Paragraph>
                                <Paragraph>{localization.AGE_LIMIT}:{filmDetailPage.adult ? " 18+" : " 0+"} </Paragraph>
                                <Paragraph>{localization.BUDGET}:{" " + filmDetailPage.budget + " $"} </Paragraph>
                            </div>
                            <div className="right-side-details">
                                <Paragraph>{localization.REVENUE}:{" " + filmDetailPage.revenue + " $"} </Paragraph>
                                {filmDetailPage.tagline ? <Paragraph>{localization.TAGLINE}:{" " + filmDetailPage.tagline} </Paragraph> : null}
                                <a rel="home-page" className="home-page" href={`${filmDetailPage?.homepage}`}> {" " + filmDetailPage?.homepage}</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="poster-image-and-icon">
                    <div className="play-icon" style={{
                        backgroundImage: `url(${`https://image.tmdb.org/t/p/original/${filmDetailPage.backdrop_path}`
                            })`, backgroundRepeat: "no-repeat", backgroundPosition: "center", backgroundSize: "cover"

                      }} >
                        <Button className="playBtn" type="text" href={filmDetailPage?.homepage} icon={<PlayCircleOutlined />} />
                    </div>
                </div>
                <Paragraph className="login">In order to leave a review, you need to  <span onClick={setSignInMenu}>log in</span> or <span onClick={setSignUpMenu}>register</span> </Paragraph>
                <Comments />
                <Comments />
                <Comments />
                <Comments />
                <Comments />
            </div>
        </div>
    );
};
export default DetailPage;
