import { setModalSignIn } from '../../modules/redux/signIn/action';
import { connect } from 'react-redux';
import DetailPage from './DetailPage';
import { setActiveButtonAC } from '../../modules/redux/signIn/action';
import { setModalSignUp } from '../../modules/redux/signUp/action';
import { ApplicationState } from '../../modules/redux';
import  {actionTypes}  from '../../modules/redux/signIn/actionTypes';
import  {actionTypesSignUp}  from '../../modules/redux/signUp/actionTypes';
import {Action, Dispatch} from 'redux';
import { IFilm } from '../../modules/redux/filmDetail/types';
import { getFilmDetail, selectGenres } from 'modules/redux/rootReducer';

export type TPropsFromState = {
    filmDetailPage: IFilm,
    genres: Array<{id: number; name: string}>
}
const mapStateToProps = (state: ApplicationState): TPropsFromState => {
    return {
        filmDetailPage: getFilmDetail(state),
        genres: selectGenres(state)
    };
};
export type TPropsFromDispatch = {
    setActiveButton:(payload:boolean) => Action<actionTypes.SET_ACTIVE_BUTTON>,
    setModalSignIn:(payload:boolean) => Action<actionTypes.SET_MODAL_SIGN_IN>,
    setModalSignUp:(payload:boolean) => Action<actionTypesSignUp.SET_MODAL_SIGN_UP>
}

const mapDispatchToProps = (dispatch: Dispatch<Action>): TPropsFromDispatch => {
    return {
      setActiveButton: (payload:boolean) => {
        return dispatch(setActiveButtonAC(payload));
      },
      setModalSignIn: (payload:boolean) => {
        return  dispatch(setModalSignIn(payload));
      },
      setModalSignUp: (payload:boolean) => {
        return  dispatch(setModalSignUp(payload));
      }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DetailPage);
