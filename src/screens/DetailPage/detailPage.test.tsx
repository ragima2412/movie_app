import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { mocks } from '../../../mocks';
import DetailPage from '../DetailPage';


describe('', () => {
    test('', () => {
        const component = render(
            <Provider store={mocks.storeDetailPage}>
                <DetailPage/>
             </Provider>
        );

        expect(component).toMatchSnapshot();
    });
});
