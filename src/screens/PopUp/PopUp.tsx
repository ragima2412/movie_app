import * as React from 'react';
import './PopUp.less';
import { FC, useState } from 'react';
import { NavLink } from 'react-router-dom';
import user from '../../assets/images/user.svg';
import { localization } from 'modules/localization';
import { TPropsFromDispatch, TPropsFromState } from '.';

type TProps = TPropsFromState & TPropsFromDispatch;

const PopUp:FC<TProps> = ({data, setInitialData, setActiveHomeLink, setActiveMoviesLink }) => {
    const [info, setInfo] = useState(true);

    const logOut = () => {
        setInfo(false);
        setInitialData();
        setActiveHomeLink(true);
        setActiveMoviesLink(false);
    };
    
    return (
        <ul className={info ? "user-info" : "user-info-none"}>
          <li className="user-name"><img src={user} className="user-icon" alt="user_image" />{data.login}</li>
          <li className="log-out" onClick={logOut}><NavLink to='/' className="log-out-link">{localization.LOG_OUT}</NavLink></li>
        </ul>
    );
};

export default PopUp;
