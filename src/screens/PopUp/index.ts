import { setActiveHomeLink, setActiveMoviesLink } from 'modules/redux/home/action';
import { setInitialDataAC } from 'modules/redux/signIn/action';
import { actionTypes } from 'modules/redux/signIn/actionTypes';
import { actionTypesHome as types } from 'modules/redux/home/actionTypes';
import { IsignInReducer } from 'modules/redux/signIn/types';
import { connect } from 'react-redux';
import {Action,Dispatch} from 'redux';
import PopUp from './PopUp';
import { ApplicationState } from '../../modules/redux';
import { getUserProfile, REDUCER } from '../../modules/redux/rootReducer';

 export type TPropsFromState = {
    data: IsignInReducer
};

const mapStateToProps = (state: ApplicationState): TPropsFromState=> {
    return {
        data: getUserProfile(state)
    };
};

export type TPropsFromDispatch = {
    setInitialData: () => Action<actionTypes.SET_INITIAL_DATA>,
    setActiveHomeLink: (parametrs: boolean) => Action<types.SET_ACTIVE_HOME_LINK>,
    setActiveMoviesLink: (parametrs: boolean) => Action<types.SET_ACTIVE_MOVIES_LINK>

}
const mapDispatchToProps = (dispatch: Dispatch<Action>):TPropsFromDispatch => {
    return {
        setInitialData: () => {
           return  dispatch(setInitialDataAC());
        },
        setActiveHomeLink: (parameters: boolean) => {
           return dispatch(setActiveHomeLink(parameters));
        },
        setActiveMoviesLink: (parameters: boolean) => {
           return dispatch(setActiveMoviesLink(parameters));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PopUp);