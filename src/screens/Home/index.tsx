import { connect } from 'react-redux';
import { Dispatch, Action } from 'redux';
import Home from './Home';
import { setActiveHomeLink, setActiveMoviesLink } from 'modules/redux/home/action';
import { setGenres, setNewReleases, setPopular, setRandom } from 'modules/saga/home/action';
import { ApplicationState } from 'modules/redux';
import { actionTypes } from 'modules/saga/home/actionTypes';
import { actionTypes as detailActionTypes} from 'modules/saga/filmDetail/actionTypes';
import { actionTypesHome } from 'modules/redux/home/actionTypes';
import { IFilm } from 'modules/redux/filmDetail/types';
import { setAllFilmsAC } from 'modules/saga/filmDetail/action';
import { selectIsLoading, selectNewReleases, selectPopular, selectRandom } from 'modules/redux/rootReducer';

export type TPropsFromState = {
    popularList: Array<IFilm>,
    newReleasesList: Array<IFilm>,
    randomList: Array<IFilm>,
    isLoading: boolean,
}

export type TPropsFromDispatch = {
    setGenres: () => Action<actionTypes.SET_GENRES>,
    setPopular: (params: {page: number, popularity: number}) => Action<actionTypes.SET_POPULAR>,
    setNewReleases: (params: {page: number}) => Action<actionTypes.SET_NEW_RELEASES>,
    setRandom: () => Action<actionTypes.SET_RANDOM>,
    setAllFilmsAC: () => Action<detailActionTypes.SET_ALL_FILMS>,
    setActiveHomeLink: (params: boolean) => Action<actionTypesHome.SET_ACTIVE_HOME_LINK>,
    setActiveMoviesLink: (params: boolean) => Action<actionTypesHome.SET_ACTIVE_MOVIES_LINK>,
}

export const mapStateToProps = (state: ApplicationState): TPropsFromState => {
    return {
        popularList: selectPopular(state),
        newReleasesList: selectNewReleases(state),
        randomList: selectRandom(state),
        isLoading: selectIsLoading(state)
    };
  };

export const mapDispatchToProps = (dispatch: Dispatch<Action>) => {
    return {
        setGenres: () => dispatch(setGenres()),
        setPopular: (params: {page: number, popularity: number}) => dispatch(setPopular(params)),
        setNewReleases: (params: {page: number}) => dispatch(setNewReleases(params)),
        setRandom: () => dispatch(setRandom()),
        setAllFilmsAC: () => dispatch(setAllFilmsAC()),
        setActiveHomeLink: (params: boolean) => dispatch(setActiveHomeLink(params)),
        setActiveMoviesLink: (params: boolean) => dispatch(setActiveMoviesLink(params))
    };
  };
  
export default connect(mapStateToProps, mapDispatchToProps)(Home);