import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { mapDispatchToProps, mapStateToProps } from '.';
import { mocks } from '../../../mocks'
import { ApplicationState } from '../../modules/redux';
import { selectIsLoading, selectNewReleases, selectRandom } from '../../modules/redux/rootReducer';
import Home from './Home';
import { selectPopularFilter } from '../../modules/redux/rootReducer';

describe('', () => {
    let state: ApplicationState;
    let dispatch;
    beforeAll(() => {
        dispatch = jest.fn();
    });
        const film = {
            id: 1,
            poster_path: "string",
            genre_ids: [1,2],
            title: "string",
            release_date:"string",
            popularity:1,
            tagline: "string",
            overview: "string",
            adult: true,
            runtime: 1,
            budget: 1,
            revenue: 1,
            homepage: "string",
            backdrop_path: "string"
        }
    
    test("", () => {
        const component = render(
            <Provider store={mocks.storeHome}>
                <Home />
            </Provider>
        );

        expect(component).toMatchSnapshot();
    });
    test("mapStateToProps test",() => {
        expect(mapStateToProps(state)).toEqual({
            popularList: selectPopularFilter(state),
            newReleasesList: selectNewReleases(state),
            randomList: selectRandom(state),
            isLoading: selectIsLoading(state)
        })
    });
    test("mapDispatchToProps setActiveHomeLink test", () => {
        mapDispatchToProps(dispatch).setActiveHomeLink(true);
        expect(dispatch.mock.calls[0].toEqual({ type: "SET_ACTIVE_HOME_LINK",
            payload:true}));
        });
    test("mapDispatchToProps setActiveMoviesLink test", () => {
        mapDispatchToProps(dispatch).setActiveMoviesLink(true);
        expect(dispatch.mock.calls[0]).toEqual({ type: "SET_ACTIVE_MOVIES_LINK",
        payload: true});
    });
    test("mapDispatchToProps setAllFilmsAC test", ()=> {
        mapDispatchToProps(dispatch).setAllFilmsAC();
        expect(dispatch.mock.calls[0].toEqual({type: "SET_ALL_FILMS"}))
    });
    test("mapDispatchToProps setGenres test", () => {
        mapDispatchToProps(dispatch).setGenres();
        expect(dispatch.mock.calls[0].toEqual({type: "SET_GENRES"}))
    });
    test("mapDispatchToProps setNewReleases test", () => {
        mapDispatchToProps(dispatch).setNewReleases({page: 1});
        expect(dispatch.mock.calls[0].toEqual({ type: "SET_NEW_RELEASES_LIST",
            payload: {page: 1}}));
    });
    test("mapDispatchToProps setPopular test", () => {
        mapDispatchToProps(dispatch).setPopular({page: 1, popularity: 1});
        expect(dispatch.mock.calls[0].toEqual({ type: "SET_POPULAR",
            payload: [film]}));
    });
    test("mapDispatchToProps setRandom test", () => {
        mapDispatchToProps(dispatch).setRandom();
        expect(dispatch.mock.calls[0].toEqual({ type: "SET_RANDOM"}));
    })
});