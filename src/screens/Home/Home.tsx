import './Home.less';
import * as React from 'react';
import { useEffect, useState } from 'react';
import Slider from 'components/Slider/Slider';
import Preloader from 'screens/Preloader/Preloader';
import RandomSlider from 'components/RandomSlider';
import Search from 'components/Search';
import { TPropsFromDispatch, TPropsFromState } from '.';

type TProps = TPropsFromState & TPropsFromDispatch;

const Home: React.FC<TProps> = ({ setGenres, setPopular, setNewReleases, setRandom, setActiveHomeLink, setActiveMoviesLink, setAllFilmsAC, 
	popularList, newReleasesList, randomList, isLoading }) => {
	const [count, setCount] = useState<number>(5);
	
	useEffect(() => {
		setGenres();
		setPopular({page: 1, popularity: 200});
		setNewReleases({page: 1});
		setRandom();
		setAllFilmsAC();
		setActiveHomeLink(true);
        setActiveMoviesLink(false);
	}, []);

	useEffect(() => {
		if (window.innerWidth > 2100) {
			setCount(6);
		} else if (window.innerWidth <= 2100 && window.innerWidth > 1600) {
			setCount(5);
		} else if (window.innerWidth <= 1600 && window.innerWidth > 1320) {
			setCount(4);
		} else if (window.innerWidth <= 1320 && window.innerWidth > 1020) {
			setCount(3);
		} else if (window.innerWidth <= 1020 && window.innerWidth > 720) {
			setCount(2);
		} else if(window.innerWidth <= 720){
			setCount(1);
	}}, [window.onresize]);
	
	const changeWidth = () => {
		if(window.innerWidth > 2100) {
			setCount(6);
		} else if(window.innerWidth <= 2100 && window.innerWidth > 1600){
			setCount(5);
		} else if(window.innerWidth <= 1600 && window.innerWidth > 1320){
			setCount(4);
		} else if(window.innerWidth <= 1320 && window.innerWidth > 1020){
			setCount(3);
		} else if(window.innerWidth <= 1020 && window.innerWidth > 720){
			setCount(2);
		} else if(window.innerWidth <= 720){
			setCount(1);
		}
	};

	window.onresize = changeWidth;

    return <>
       {isLoading ? <Preloader /> : 
		<div className="main">
			<RandomSlider list={randomList}/>
				<div className="home-search">
					<Search />
				</div>   
				<div className="popular-text">Popular</div>
				<div id="popular-slider">
					<Slider list={popularList} numberOfCards={count}/>
				</div> 
				<div className="new-text">New</div>
				<div id="new-releases-slider">
					<Slider list={newReleasesList} numberOfCards={count}/>
				</div> 
        </div>
		}
    </>;
};

export default Home;
