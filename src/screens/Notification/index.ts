import { setIsLoadingMessageSignIn, setMessageSignIn } from 'modules/redux/signIn/action';
import { setIsLoadingMessageAC, setMessageAC } from 'modules/redux/signUp/action';
import { Dispatch, Action } from 'redux';
import { connect } from 'react-redux';
import Notification from './Notification';
import { actionTypesSignUp } from 'modules/redux/signUp/actionTypes';
import { actionTypes } from 'modules/redux/signIn/actionTypes';
import { ApplicationState } from 'modules/redux';
import { getUserData, messageSelector } from '../../modules/redux/rootReducer';

export type TPropsFromDispatch = {
    setIsLoadingMessageAC: (parameters: boolean) => Action<actionTypesSignUp.SET_IS_LOADING_MESSAGE>,
    setIsLoadingMessageSignIn: (parameters: boolean) => Action<actionTypes.SET_IS_LOADING_MESSAGE_SIGN_IN>,
    setMessageAC: (payload: {first_name: '', last_name: '', login: '', password: '',  message: ''}) => Action<actionTypesSignUp.SET_MESSAGE>,
    setMessageSignIn: (payload: {login: '', password: '', message: ''}) => Action<actionTypes.SET_MESSAGE_SIGN_IN>
};

export type TPropsFromState = {
    message: {
        first_name: string,
        last_name: string,
        login: string,
        password: string,
        message: string,
    },
    messageSignIn: {
        login: string,
        password: string,
        message: string,
    },
}

export type TPassedProps = {
    mes : {
        first_name?: string,
        last_name?: string,
        login: string,
        password: string,
        message: string,
    }
}

const mapStateToProps = (state: ApplicationState): TPropsFromState => {
    return {
        message: messageSelector(state),
        messageSignIn: getUserData(state)
    };
};

const mapDispatchToProps = (dispatch: Dispatch) : TPropsFromDispatch => {
    return {    
        setIsLoadingMessageAC: (parameters: boolean) => {
            return dispatch(setIsLoadingMessageAC(parameters));
        },
        setIsLoadingMessageSignIn: (parameters: boolean) => {
            return dispatch(setIsLoadingMessageSignIn(parameters));
        },
        setMessageAC: (payload: {first_name: '', last_name: '', login: '', password: '',  message: ''}) => {
            return dispatch(setMessageAC(payload));
        },
        setMessageSignIn: (payload: {login: '', password: '', message: ''}) => {
            return dispatch(setMessageSignIn(payload));
        }
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(Notification);