import './Notification.less';
import * as React from 'react';
import { useEffect } from 'react';
import { TPassedProps, TPropsFromDispatch, TPropsFromState } from '.';

type TProps = TPropsFromState & TPropsFromDispatch & TPassedProps;

const Notification : React.FC<TProps> = ({mes}, {setIsLoadingMessageAC, setIsLoadingMessageSignIn, setMessageAC, setMessageSignIn}) => {

    useEffect(() => {
        setTimeout(() => {
            setMessageAC({first_name: '', last_name: '', login: '', password: '',  message: ''});  
        }, 3000);
    }, [mes.first_name, mes.last_name, mes.login, mes.password, mes.message]);

    useEffect(() => {
        setTimeout(() => {
            setMessageSignIn({login: '', password: '', message: ''});
        }, 3000);
    }, [mes.message, mes.password, mes.login]);

    const close = () => {
        setIsLoadingMessageAC(false);
        setIsLoadingMessageSignIn(false);
    };

    return (
        <div className="notification-wrapper">
            <p className="close" onClick={close}>X</p>
            {mes.first_name && <p className="notification-message">{mes.first_name}</p>}
            {mes.last_name && <p className="notification-message">{mes.last_name}</p>}
            {mes.login && <p className="notification-message">{mes.login}</p>}
            {mes.password && <p className="notification-message">{mes.password}</p>}
            {mes.message && <p className="notification-message">{mes.message}</p>}
        </div>
    );
};

export default Notification;

