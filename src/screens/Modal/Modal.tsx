import './Modal.less';
import * as React from 'react';
import { Modal, Button, Form, Input, Typography } from 'antd';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import { localization } from '../../modules/localization';
import Notification from '../../screens/Notification';
import { useEffect } from 'react';
import { TPropsFromDispatch, TPropsFromState } from '.';

const { Title } = Typography;
const { Item } = Form;

type TProps = TPropsFromState & TPropsFromDispatch;

const ModalWindow: React.FC<TProps> = ({
    setSignUpInfo,
    setLastNameChangeSignUp,
    setFirstNameChangeSignUp,
    setPasswordChangeSignUp,
    setOnChangeLoginSignUp,
    setSignInInfo,
    setIsLoadingMessageSignIn,
    setIsLoadingMessageSignUp,
    setLogin,
    setPassword,
    setEmptyMessageSignUp,
    setActiveButton,
    setModalSignUp,
    setModalSignIn,
    setEmptyMessageSignInInputs,
    activeButton,
    isLoadingMessageSignIn,
    messageSignIn,
    userProfileSignIn,
    modalWindowSignIn,
    modalWindowSignUp,
    messageSignUp,
    isLoadingMessageSignUp,
    userProfileSignUp
}) => {
    useEffect(() => {
        setEmptyMessageSignInInputs({login: '', password: ''});
    }, []);

    const closeModal = () => {
        setModalSignIn(false);
        setModalSignUp(false);
    };

    const changeSingUp = () => {
        setEmptyMessageSignInInputs({login: '', password: ''});
        setModalSignUp(false);
        setActiveButton(false);
    };

    const changeSingIn = () => {
        setEmptyMessageSignUp({first_name: '', last_name: '', login: '', password: ''});
        setActiveButton(true);
        setModalSignUp(false);
    };

    const onLoginChange = (e: { target: { value: string } }) => {
        setLogin(e.target.value);
    };

    const onPasswordChange = (e: { target: { value: string } }) => {
        setPassword(e.target.value);
    };

    const setLoginHandleClick = () => {
        setSignInInfo({...userProfileSignIn, login: userProfileSignIn.login, password: userProfileSignIn.password});
        setLogin(userProfileSignIn.login);
        setIsLoadingMessageSignIn(true);
        setTimeout(() => {
            setIsLoadingMessageSignIn(false);
        }, 3000);
        setIsLoadingMessageSignUp(false);

    };

    const onNameChangeSignUp = (e: { target: { value: string } }) => {
        setOnChangeLoginSignUp(e.target.value);
    };

    const onPasswordChangeSignUp = (e: { target: { value: string } }) => {
        setPasswordChangeSignUp(e.target.value);
    };

    const onFirstNameChangeSignUp = (e: { target: { value: string } }) => {
        setFirstNameChangeSignUp(e.target.value);
    };

    const onLastNameChangeSignUp = (e: { target: { value: string } }) => {
        setLastNameChangeSignUp(e.target.value);
    };

    const setRegistr = () => {
        setSignUpInfo({
            ...userProfileSignUp,
            first_name: userProfileSignUp.first_name,
            last_name: userProfileSignUp.last_name,
            login: userProfileSignUp.login,
            password: userProfileSignUp.password
        });
        setIsLoadingMessageSignUp(true);
        setTimeout(() => {
            setIsLoadingMessageSignUp(false);
        }, 3000);
        setIsLoadingMessageSignIn(false);
    };

    return (
        <>
            <Modal
                className="registration"
                title="Registration"
                visible={modalWindowSignIn}
                onCancel={closeModal}
            >
                <Form>
                    <Item>
                        {modalWindowSignUp || !activeButton
                            ? <Title className="registration-title" level={3}>{localization.REGISTRATION}</Title>
                            : <Title className="registration-title" level={3}>{localization.AUTHORIZATION}</Title>
                        }
                    </Item>
                    <Item className="buttonsItem">
                        <Button onClick={changeSingIn}
                                className={activeButton ? "button-toggle-active" : "button-toggle"}>{localization.SIGN_IN}</Button>
                        <Button onClick={changeSingUp}
                                className={activeButton ? "button-toggle" : "button-toggle-active"}>{localization.SIGN_UP}</Button>
                    </Item>
                    {modalWindowSignUp || !activeButton
                        ? <>
                            <Item className="login-wrapper">
                                <Input className="registration-inputs" value={userProfileSignUp.first_name}
                                       maxLength={15} name="sign_up" onChange={onFirstNameChangeSignUp}/>
                                <Title className="login-title" level={5}>{localization.FIRST_NAME}</Title>
                            </Item>
                            <Item className="password-wrapper">
                                <Input className="registration-inputs" value={userProfileSignUp.last_name}
                                       name="sign_up" maxLength={15} onChange={onLastNameChangeSignUp}/>
                                <Title className="password-title" level={5}>{localization.LAST_NAME}</Title>
                            </Item>
                            <Item className="password-wrapper">
                                <Input className="registration-inputs" value={userProfileSignUp.login} name="sign_up"
                                       maxLength={15} onChange={onNameChangeSignUp}/>
                                <Title className="password-title" level={5}>{localization.LOGIN}</Title>
                            </Item>
                            <Item className="password-wrapper">
                                <Input.Password
                                    onChange={onPasswordChangeSignUp}
                                    value={userProfileSignUp.password}
                                    className="input-password input-password registration-inputs"
                                    iconRender={visible => visible ? <EyeTwoTone/> : <EyeInvisibleOutlined/>}
                                />
                                <Title className="password-title" level={5}>{localization.PASSWORD}</Title>
                            </Item>
                        </>
                        : <>
                            <Item className="login-wrapper">
                                <Input onChange={onLoginChange} value={userProfileSignIn.login}
                                       className="registration-inputs" maxLength={15} name="sign_in"/>
                                <Title className="login-title" level={5}>{localization.LOGIN}</Title>
                            </Item>
                            <Item className="password-wrapper">
                                <Input.Password
                                    onChange={onPasswordChange}
                                    value={userProfileSignIn.password}
                                    className="input-password registration-inputs"
                                    iconRender={visible => visible ? <EyeTwoTone/> : <EyeInvisibleOutlined/>}
                                />
                                <Title className="password-title" level={5}>{localization.PASSWORD}</Title>
                            </Item>
                        </>
                    }
                </Form>
                {modalWindowSignUp || !activeButton
                    ? <Button disabled={isLoadingMessageSignUp} type="primary"
                              className="sign-in-sign-up-buttons" onClick={setRegistr}>{localization.SIGN_UP}</Button>
                    : <Button disabled={isLoadingMessageSignIn} type="primary"
                              className="sign-in-sign-up-buttons"
                              onClick={setLoginHandleClick}>{localization.SIGN_IN}</Button>
                }
            </Modal>
            {isLoadingMessageSignUp && (messageSignUp.login || messageSignUp.first_name || messageSignUp.last_name || messageSignUp.password || messageSignUp.message) ?
                <Notification mes={messageSignUp}/> : null}
            {isLoadingMessageSignIn && (messageSignIn.login || messageSignIn.password || messageSignIn.message) ?
                <Notification mes={messageSignIn}/> : null}
        </>
    );
};

export default ModalWindow;