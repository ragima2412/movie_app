import { ApplicationState } from 'modules/redux';
import { setActiveButtonAC, setEmptyMessageSignInInputs, setIsLoadingMessageSignIn, setLoginAC, setModalSignIn, setPasswordAC } from 'modules/redux/signIn/action';
import { IsignInReducer } from 'modules/redux/signIn/types';
import { setEmptyMessageSignUp, setFirstNameSignUp, setIsLoadingMessageAC, setLastNameSignUp, setLoginSignUp, setModalSignUp, setPasswordSignUp } from 'modules/redux/signUp/action';
import { signInAC } from 'modules/saga/signIn/action';
import { signUpAC } from 'modules/saga/signUp/action';
import { Dispatch, Action } from 'redux';
import { connect } from 'react-redux';
import ModalWindow from './Modal';
import { actionTypes } from 'modules/redux/signIn/actionTypes';
import { actionTypesSignUp } from 'modules/redux/signUp/actionTypes';
import { actionTypesSignInSaga } from 'modules/saga/signIn/actionTypes';
import { actionTypesSingUpSaga } from 'modules/saga/signUp/actionTypes';
import {
    getModalWindow,
    getUserData,
    getUserProfile,
    isLoadingMessageSignIn,
    setActiveButton,
    modalSelector,
    isLoadingMessage,
    isUserInfo,
    messageSelector
} from '../../modules/redux/rootReducer';

export type TPropsFromState = {
    activeButton: boolean,
    isLoadingMessageSignIn: boolean,
    messageSignIn: {
        login:string,
        password:string,
        message:string,
    },
    userProfileSignIn: IsignInReducer,
    modalWindowSignIn: boolean,
    modalWindowSignUp: boolean,
    messageSignUp: {
        first_name: string,
        last_name: string,
        login: string,
        password: string,
        message: string,
    },
    isLoadingMessageSignUp: boolean,
    userProfileSignUp: {
        first_name: string,
        last_name: string,
        login: string,
        password: string
    }
}

export type TPropsFromDispatch = {
    setEmptyMessageSignInInputs: (payload :{login: string, password: string}) => Action<actionTypes.SET_EMPTY_MESSAGE_SIGN_IN_INPUTS>,
    setModalSignIn: (parameters: boolean) => Action<actionTypes.SET_MODAL_SIGN_IN>,
    setModalSignUp: (parameters: boolean) => Action<actionTypesSignUp.SET_MODAL_SIGN_UP>,
    setActiveButton: (parameters: boolean) => Action<actionTypes.SET_ACTIVE_BUTTON>,
    setEmptyMessageSignUp: (payload :{first_name: string, last_name: string, login: string, password: string}) => Action<actionTypesSignUp.SET_EMPTY_MESSAGE_SIGN_UP>,
    setLogin: (login: string) => Action<actionTypes.SET_LOGIN>,
    setPassword: (password : string) => Action<actionTypes.SET_PASSWORD>,
    setSignInInfo: (info : {login: string; password : string}) => Action<actionTypesSignInSaga.SIGN_IN>,
    setIsLoadingMessageSignIn: (parameters: boolean) => Action<actionTypes.SET_IS_LOADING_MESSAGE_SIGN_IN>,
    setIsLoadingMessageSignUp: (parameters: boolean) => Action<actionTypesSignUp.SET_IS_LOADING_MESSAGE>,
    setOnChangeLoginSignUp: (login: string) => Action<actionTypesSignUp.SET_LOGIN_SIGN_UP>,
    setPasswordChangeSignUp: (password : string) => Action<actionTypesSignUp.SET_PASSWORD_SIGN_UP>,
    setFirstNameChangeSignUp: (first_name: string) => Action<actionTypesSignUp.SET_FIRST_NAME_SIGN_UP>,
    setLastNameChangeSignUp: (last_name: string) => Action<actionTypesSignUp.SET_LAST_NAME_SIGN_UP>,
    setSignUpInfo: (info: {
        first_name: string,
        last_name: string,
        login: string,
        password: string
    }) => Action<actionTypesSingUpSaga.SET_AUTH>
}

const mapStateToProps = (state: ApplicationState) : TPropsFromState => {
    return {
        activeButton: setActiveButton(state),
        isLoadingMessageSignIn: isLoadingMessageSignIn(state),
        messageSignIn: getUserData(state),
        userProfileSignIn: getUserProfile(state),
        modalWindowSignIn: getModalWindow(state),
        modalWindowSignUp: modalSelector(state),
        messageSignUp: messageSelector(state),
        isLoadingMessageSignUp: isLoadingMessage(state),
        userProfileSignUp: isUserInfo(state)
    };
};

const mapDispatchToProps = (dispatch: Dispatch): TPropsFromDispatch => {
    return {
        setEmptyMessageSignInInputs: (payload :{login: string, password: string}) => {
           return dispatch(setEmptyMessageSignInInputs(payload));
        },
        setModalSignIn: (parameters: boolean) => {
            return  dispatch(setModalSignIn(parameters));
        },
        setModalSignUp: (parameters: boolean) => {
            return  dispatch(setModalSignUp(parameters));
        },
        setActiveButton: (parameters: boolean) => {
            return  dispatch(setActiveButtonAC(parameters));
        },
        setEmptyMessageSignUp: (payload :{first_name: string, last_name: string, login: string, password: string}) => {
            return dispatch(setEmptyMessageSignUp(payload));
        },
        setLogin: (login: string) => {
            return  dispatch(setLoginAC(login));
        },
        setPassword: (password : string) => {
            return dispatch(setPasswordAC(password));
        },
        setSignInInfo: (info : {login: string; password : string}) => {
            return dispatch(signInAC(info));
        },
        setIsLoadingMessageSignIn: (parameters: boolean) => {
            return  dispatch(setIsLoadingMessageSignIn(parameters));
        },
        setIsLoadingMessageSignUp: (parameters: boolean) => {
            return dispatch(setIsLoadingMessageAC(parameters));
        },
        setOnChangeLoginSignUp: (login: string) => {
            return  dispatch(setLoginSignUp(login));
        },
        setPasswordChangeSignUp: (password : string) => {
            return dispatch(setPasswordSignUp(password));
        },
        setFirstNameChangeSignUp: (first_name: string) => {
            return dispatch(setFirstNameSignUp(first_name));
        },
        setLastNameChangeSignUp: (last_name: string) => {
            return  dispatch(setLastNameSignUp(last_name));
        },
        setSignUpInfo: (info: {
            first_name: string,
            last_name: string,
            login: string,
            password: string
        }) => {
            return  dispatch(signUpAC(info));
        }
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(ModalWindow);
