import { connect } from 'react-redux';
import { Dispatch, Action } from 'redux';
import { setFilms, setFirstFilms, setGenres } from 'modules/saga/filmsList/action';
import { setActiveHomeLink, setActiveMoviesLink } from 'modules/redux/home/action';
import FilmsList from './FilmList';
import { ApplicationState } from 'modules/redux';
import { actionTypes } from 'modules/saga/filmsList/actionTypes';
import { actionTypesHome } from 'modules/redux/home/actionTypes';
import { selectCurrentPage, selectFilms, selectFilteredData, selectIsFiltered, selectIsLoading, selectTotalCount } from 'modules/redux/rootReducer';
import { IFilm } from 'modules/redux/filmDetail/types';

export type TPropsFromState = {
  filmsList: Array<IFilm>,
  totalCount: number,
  currentPage: number,
  isFiltered: boolean,
  filteredData: Array<IFilm>,
  isLoading: boolean, 
}

export type TPropsFromDispatch = {
  setGenres: () => Action<actionTypes.SET_GENRES>,
  setFilms: (currentPage: number) => Action<actionTypes.SET_FILMS>,
  setFirstFilms: (currentPage: number) => Action<actionTypes.SET_FIRST_FILMS>,
  setActiveHomeLink: (params: boolean) => Action<actionTypesHome.SET_ACTIVE_HOME_LINK>,
  setActiveMoviesLink: (params: boolean) => Action<actionTypesHome.SET_ACTIVE_MOVIES_LINK>,
}

const mapStateToProps = (state: ApplicationState): TPropsFromState => {
    return {
      filmsList: selectFilms(state),
      totalCount: selectTotalCount(state),
      currentPage: selectCurrentPage(state),
      isFiltered: selectIsFiltered(state),
      filteredData: selectFilteredData(state),
      isLoading: selectIsLoading(state),
    };
  };
  
const mapDispatchToProps = (dispatch: Dispatch<Action>): TPropsFromDispatch => {
    return {
      setGenres: () => {
       return dispatch(setGenres());
      },
      setActiveHomeLink: (params: boolean) => {
        return dispatch(setActiveHomeLink(params));
      },
      setActiveMoviesLink: (params: boolean) => {
        return dispatch(setActiveMoviesLink(params));
      },
      setFilms: (currentPage: number) => {
        return dispatch(setFilms(currentPage));
      },
      setFirstFilms: (currentPage: number) => {
        return dispatch(setFirstFilms(currentPage));
      },
    };
  };
  
export default connect(mapStateToProps, mapDispatchToProps)(FilmsList);