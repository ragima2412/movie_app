import './FilmList.less';
import { useEffect } from 'react';
import * as React from 'react';
import Filters from 'components/FilterButtons';
import FilmLink from 'components/FilmLink';
import LoadMoreButton from 'components/LoadMore/LoadMore';
import { routes } from 'services/routes';
import Preloader from 'screens/Preloader/Preloader';
import { IFilm } from 'modules/redux/filmDetail/types';
import { TPropsFromState, TPropsFromDispatch } from '.';

type TProps = TPropsFromState & TPropsFromDispatch;

const FilmsList: React.FC<TProps> = ({
  filmsList,
  totalCount,
  currentPage,
  isFiltered,
  filteredData,
  isLoading,
  setGenres,
  setActiveHomeLink,
  setActiveMoviesLink,
  setFirstFilms,
  setFilms
}) => {
    let totalPages: number = totalCount / 20;
    let data: Array<IFilm>;

    if (isFiltered) {
        data = filteredData;
        totalPages = currentPage - 1;
    } else {
        data = filmsList;
    }

    useEffect(() => {
        setGenres();
        setActiveHomeLink(false);
        setActiveMoviesLink(true);
        if (currentPage === 1) {
            setFirstFilms(currentPage);
        }
    }, []);

    const getMore = () => {
        if (currentPage <= totalPages || currentPage === 1) {
            setFilms(currentPage);
        }
    };

    return <>
        {isLoading ? <Preloader/> :
            <div className="films">
                <Filters/>
                <div className="list">
                    {data?.length ? data?.map((item: IFilm) => (
                            <FilmLink item={item} key={item.id}/>
                        )) :
                        <div className="no-data">{routes.noData}</div>
                    }
                </div>
                <LoadMoreButton currentPage={currentPage} totalPages={totalPages} getMore={getMore}/>
            </div>
        }
    </>;
};

export default FilmsList;
