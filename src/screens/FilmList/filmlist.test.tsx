import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { mocks } from '../../../mocks';
import FilmsList from './FilmList';


describe('filmlist test', () => {
    test('filmlist test', () => {
        const component = render(
            <Provider store={mocks.storeFilmList}>
                <FilmsList />
             </Provider>
        );

        expect(component).toMatchSnapshot();
    });
});