import * as React from 'react';
import DetailPage from 'screens/DetailPage';
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';
import Home from 'screens/Home';
import FilmsList from 'screens/FilmList';
import { useSelector } from 'react-redux';
import { selectIsFiltered } from './modules/redux/rootReducer';
import Footer from 'components/Footer/Footer';
import Header from 'components/Header';
import { getUserProfile } from './modules/redux/rootReducer';

const App = () => {
  const token = useSelector(getUserProfile);
  const isFiltered = useSelector(selectIsFiltered);

  return (
    <BrowserRouter>
        <Header />
        <Routes>
          {token?.token ?  <Route path="*" element={<Navigate to ="/" />}/> : null}
          <Route path='/movies' element={<FilmsList/>}/>
          {isFiltered ?  <Route path="*" element={<Navigate to ="/movies" />}/> : <Route path='/' element={<Home/>}/>}
          <Route path='/film' element={<DetailPage/>}/>
        </Routes>
        <Footer/>
    </BrowserRouter> 
  );
};

export default App;
