import configureStore from 'redux-mock-store';

const mockStore = configureStore();

const storeFilters = mockStore({
    commonReducer: {
        genres: [{ id: 1, name: 'name' }],
        languages: [{ value: 'value', name: 'name' }]
    },
    filtersReducer: {
        value: ['value'],
        budget: ['budget'],
        popular: ['popular'],
        revenue: ['revenue'],
        search: 'search',
        years: ['years']
    }
});

export const storeFilmList = mockStore({
    filmsReducer: {
        filmsList: [{}],
        totalCount: 1,
        currentPage: 1,
        isLoading: true,
        isFiltered: true,
        filteredData: [{}]
    },
    commonReducer: {
        genres: [{ id: 1, name: 'name' }],
        languages: [{ value: 'value', name: 'name' }]
    }
});

export const storeDetailPage = mockStore({
    filmDetailReducer: {
        filmDetail: {
            id: 1,
            poster_path: 'poster_path',
            genre_ids: [1, 2],
            title: 'title',
            release_date: 'release_date',
            popularity: 1,
            tagline: 'tagline',
            overview: 'overview',
            adult: 'adult',
            runtime: 1,
            budget: 'budget',
            revenue: 'revenue',
            homepage: 'homepage',
            backdrop_path: 'backdrop_path',
        },
        allFilmsList: {
            id: 1,
            poster_path: 'poster_path',
            genre_ids: [1, 2],
            title: 'title',
            release_date: 'release_date',
            popularity: 1,
            tagline: 'tagline',
            overview: 'overview',
            adult: 'adult',
            runtime: 1,
            budget: 'budget',
            revenue: 'revenue',
            homepage: 'homepage',
            backdrop_path: 'backdrop_path',
        }
    },
    commonReducer: {
        genres: [{ id: 1, name: 'name' }],
        languages: [{ value: 'value', name: 'name' }]
    },
});

export const storeHome = mockStore({
    sliderReducer: {
        popularList: [{
            poster_path: 'poster_path',
            genre_ids: [1, 2],
            title: 'title',
        }],
        newReleasesList: [{
            poster_path: 'poster_path',
            genre_ids: [1, 2],
            title: 'title',
        }],
        randomList: [{
            backdrop_path: 'backdrop_path',
            poster_path: 'poster_path',
            genre_ids: [1, 2],
            title: 'title',
        }],
        isLoading: true
    },
});
export const storeHeader = mockStore({
    signInReducer: {
        login: 'login',
        token: 'token',
        message: {
            login: 'login',
            password: 'password',
            message: 'message',
            modalWindow: true
        },
        userId: 'userId',
        password: 'password',
        modalWindow: true,
        logOut: true,
        activeButton: true,
        isLoadingMessage: true
    },
    sliderReducer: {
        popularList: [{
            poster_path: 'poster_path',
            genre_ids: [1, 2],
            title: 'title',
        }],
        newReleasesList: [{
            poster_path: 'poster_path',
            genre_ids: [1, 2],
            title: 'title',
        }],
        randomList: [{
            backdrop_path: 'backdrop_path',
            poster_path: 'poster_path',
            genre_ids: [1, 2],
            title: 'title',
        }],
        isLoading: true
    },
    signUpReducer: {
        message: {
            first_name: 'first_name',
            last_name: 'last_name',
            login: 'login',
            password: 'password',
            message: 'message',
        },
        modalWindow: false,
        isLoadingMessage: false
    }
});

export default storeFilters;
