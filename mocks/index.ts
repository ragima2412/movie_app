import storeFilters, { storeFilmList, storeDetailPage, storeHome, storeHeader } from './mockStore';

export const mocks = {
    storeFilters,
    storeFilmList,
    storeDetailPage,
    storeHome,
    storeHeader
}
